package resources;

/**
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 */
public class Resource {
	private String subject;
	private String predicate;
	private String object;

	public Resource() {
		super();
	}

	public Resource(String subject, String predicate, String object) {
		super();
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
	}

	public void displayResource() {
		System.out.println("\n" + subject + "\t " + predicate + "\t " + object);

	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getPredicate() {
		return predicate;
	}

	public void setPredicate(String predicate) {
		this.predicate = predicate;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

}
