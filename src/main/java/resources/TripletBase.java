package resources;

import java.util.LinkedList;
import java.util.List;

/**
 * 
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 *
 */
public class TripletBase {

	private static TripletBase instance = null;
	private List<Resource> allResources;

	private TripletBase() {
		super();
		allResources = new LinkedList<Resource>();
	}

	public static TripletBase getInstance() {
		if (instance == null) {
			instance = new TripletBase();
		}
		return instance;
	}

	public void displayAllResources() {
		for (Resource resource : allResources) {
			resource.displayResource();
		}
	}

	public void addResource(Resource resource) {
		this.allResources.add(resource);
	}

	public List<Resource> getAllResources() {
		return allResources;
	}

	public void setAllResources(List<Resource> allResources) {
		this.allResources = allResources;
	}

}
