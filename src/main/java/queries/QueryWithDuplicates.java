package queries;

import java.util.Objects;

import org.eclipse.rdf4j.query.parser.ParsedQuery;

public class QueryWithDuplicates {
	
	private ParsedQuery query;
	private int nbDuplicates;

	public QueryWithDuplicates(ParsedQuery query) {
		super();
		this.query = query;
		this.nbDuplicates = 0;
	}
	
	public void incrementDuplications() {
		this.nbDuplicates++;
	}

	public ParsedQuery getQuery() {
		return query;
	}

	public void setQuery(ParsedQuery query) {
		this.query = query;
	}

	public int getNbDuplicates() {
		return nbDuplicates;
	}

	public void setNbDuplicates(int nbDuplicates) {
		this.nbDuplicates = nbDuplicates;
	}


	
	
	
}
