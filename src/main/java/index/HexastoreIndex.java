package index;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 */
public abstract class HexastoreIndex {
	Map<Long, Map<Long, List<Long>>> mapIndex;

	public HexastoreIndex() {
		this.mapIndex = new HashMap<Long, Map<Long, List<Long>>>();
	}

	public abstract void addIndexElement(long indSubject, long indPredicate, long indObject);

	public void displayIndex() {
		Map<Long, List<Long>> tempMap = null;
		for (Long ind1 : mapIndex.keySet()) {
			tempMap = mapIndex.get(ind1);
			for (Long ind2 : tempMap.keySet()) {
				for (Long ind3 : tempMap.get(ind2)) {
					System.out.println("( " + ind1 + " , " + ind2 + " , " + ind3 + " )");
				}
			}
		}
	}

}
