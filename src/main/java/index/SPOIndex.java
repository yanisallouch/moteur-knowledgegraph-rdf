package index;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 */

public class SPOIndex extends HexastoreIndex {

	public SPOIndex() {
		super();
	}

	@Override
	public void addIndexElement(long indSubject, long indPredicate, long indObject) {

		Map<Long, List<Long>> tempMap = null;
		List<Long> objects = null;
		if (!mapIndex.containsKey(indSubject)) {
			tempMap = new HashMap<Long, List<Long>>();
			objects = new LinkedList<Long>();
		} else {
			tempMap = mapIndex.get(indSubject);
			if (!tempMap.containsKey(indPredicate)) {
				objects = new LinkedList<Long>();
			} else {
				objects = tempMap.get(indPredicate);
			}
		}
		objects.add(indObject);
		tempMap.put(indPredicate, objects);
		mapIndex.put(indSubject, tempMap);

	}

}
