package index;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 */

public class OSPIndex extends HexastoreIndex {

	public OSPIndex() {
		super();
	}

	@Override
	public void addIndexElement(long indSubject, long indPredicate, long indObject) {
		Map<Long, List<Long>> tempMap = null;
		List<Long> predicates = null;

		if (!mapIndex.containsKey(indObject)) {
			tempMap = new HashMap<Long, List<Long>>();
			predicates = new LinkedList<Long>();
		} else {
			tempMap = mapIndex.get(indObject);
			if (!tempMap.containsKey(indSubject)) {
				predicates = new LinkedList<Long>();
			} else {
				predicates = tempMap.get(indSubject);
			}
		}
		predicates.add(indPredicate);
		tempMap.put(indSubject, predicates);
		mapIndex.put(indObject, tempMap);

	}

}
