package index;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 */

public class POSIndex extends HexastoreIndex {

	public POSIndex() {
		super();
	}

	@Override
	public void addIndexElement(long indSubject, long indPredicate, long indObject) {
		Map<Long, List<Long>> tempMap = null;
		List<Long> subjects = null;

		if (!mapIndex.containsKey(indPredicate)) {
			tempMap = new HashMap<Long, List<Long>>();
			subjects = new LinkedList<Long>();
		} else {
			tempMap = mapIndex.get(indPredicate);
			if (!tempMap.containsKey(indObject)) {
				subjects = new LinkedList<Long>();
			} else {
				subjects = tempMap.get(indObject);
			}
		}
		subjects.add(indSubject);
		tempMap.put(indObject, subjects);
		mapIndex.put(indPredicate, tempMap);

	}

	public Set<Long> getListSubjects(long indPredicate, long indObjects) {
		Map<Long, List<Long>> objects = mapIndex.get(indPredicate);
		List<Long> results = objects.get(indObjects);
		if (results != null) {
			return new HashSet<Long>(results);
		} else {
			return null;
		}
	}

}
