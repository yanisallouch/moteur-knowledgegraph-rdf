package index;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 */

public class OPSIndex extends HexastoreIndex {

	public OPSIndex() {
		super();
	}

	@Override
	public void addIndexElement(long indSubject, long indPredicate, long indObject) {
		Map<Long, List<Long>> tempMap = null;
		List<Long> subjects = null;

		if (!mapIndex.containsKey(indObject)) {
			tempMap = new HashMap<Long, List<Long>>();
			subjects = new LinkedList<Long>();
		} else {
			tempMap = mapIndex.get(indObject);
			if (!tempMap.containsKey(indPredicate)) {
				subjects = new LinkedList<Long>();
			} else {
				subjects = tempMap.get(indPredicate);
			}
		}
		subjects.add(indSubject);
		tempMap.put(indPredicate, subjects);
		mapIndex.put(indObject, tempMap);

	}

	public Set<Long> getListSubjects(long indPredicate, long indObjects) {
		Map<Long, List<Long>> predicates = mapIndex.get(indObjects);
		List<Long> results = predicates.get(indPredicate);
		if (results != null) {
			return new HashSet<Long>(results);
		} else {
			return null;
		}
	}

}
