package index;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 */

public class SOPIndex extends HexastoreIndex {

	public SOPIndex() {
		super();
	}

	@Override
	public void addIndexElement(long indSubject, long indPredicate, long indObject) {
		Map<Long, List<Long>> tempMap = null;
		List<Long> predicates = null;
		if (!mapIndex.containsKey(indSubject)) {
			tempMap = new HashMap<Long, List<Long>>();
			predicates = new LinkedList<Long>();
		} else {
			tempMap = mapIndex.get(indSubject);
			if (!tempMap.containsKey(indObject)) {
				predicates = new LinkedList<Long>();
			} else {
				predicates = tempMap.get(indObject);
			}
		}
		predicates.add(indPredicate);
		tempMap.put(indObject, predicates);
		mapIndex.put(indSubject, tempMap);

	}

}
