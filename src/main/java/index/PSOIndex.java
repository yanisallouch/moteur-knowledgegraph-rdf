package index;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 */

public class PSOIndex extends HexastoreIndex {

	public PSOIndex() {
		super();
	}

	@Override
	public void addIndexElement(long indSubject, long indPredicate, long indObject) {
		Map<Long, List<Long>> tempMap = null;
		List<Long> objects = null;

		if (!mapIndex.containsKey(indPredicate)) {
			tempMap = new HashMap<Long, List<Long>>();
			objects = new LinkedList<Long>();
		} else {
			tempMap = mapIndex.get(indPredicate);
			if (!tempMap.containsKey(indSubject)) {
				objects = new LinkedList<Long>();
			} else {
				objects = tempMap.get(indSubject);
			}
		}
		objects.add(indObject);
		tempMap.put(indSubject, objects);
		mapIndex.put(indPredicate, tempMap);

	}

}
