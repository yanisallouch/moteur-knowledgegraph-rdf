package timer;

import org.apache.commons.lang3.time.StopWatch;

public class Timer {
	private StopWatch stopWatch;

	public Timer() {
		super();
		stopWatch = new StopWatch();
	}

	public void startTimer() {
		stopWatch = new StopWatch();
		stopWatch.start();
	}

	public void endTimer() {
		stopWatch.stop();
	}

	public void pauseTimer() {
		stopWatch.suspend();
	}

	public void resumeTimer() {
		stopWatch.resume();
	}

	public long getTimeMilliseconds() {
		if (Long.compare(stopWatch.getTime(), 0) == 0) {
			return 1;
		} else {
			return stopWatch.getTime();
		}
	}
}
