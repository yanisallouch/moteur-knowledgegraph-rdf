package qengineWithJena;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;

import timer.Timer;

public class JenaParseQueriesProcessor {

	private Timer timerReadQueries;
	public JenaParseQueriesProcessor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<Query> parseQueries(String queryFile) throws IOException {
	
		List<Query> queries = new LinkedList<Query>();
		
		try (Stream<String> lineStream = Files.lines(Paths.get(queryFile))) {
			Iterator<String> lineIterator = lineStream.iterator();
			StringBuilder queryString = new StringBuilder();
			
			timerReadQueries = new Timer();
			timerReadQueries.startTimer();
			
			while (lineIterator.hasNext()) {
				String line = lineIterator.next();
				queryString.append(line);

				if (line.trim().endsWith("}")) {

					queries.add(QueryFactory.create(queryString.toString()));
					queryString.setLength(0);
				}
			}
			timerReadQueries.endTimer();
		}
		  
		return queries;
	}

	public Timer getTimerReadQueries() {
		return timerReadQueries;
	}

	public void setTimerReadQueries(Timer timerReadQueries) {
		this.timerReadQueries = timerReadQueries;
	}
  
}
