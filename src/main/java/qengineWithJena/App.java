package qengineWithJena;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;

import timer.Timer;
import utilities.Utility;

public class App {

	static final String baseURI = null;

	/**
	 * Votre répertoire de travail où vont se trouver les fichiers à lire
	 */
	static String workingDir = "data" + File.separator;
	/**
	 * répertoire de travail où vont se trouver les fichiers résultats de
	 * l'execution
	 */
	static String baseOutputWorkingDir = "results" + File.separator;

	/**
	 * Fichier contenant les requêtes sparql
	 */
	static String queryFile = workingDir + "";

	/**
	 * Fichier contenant des données rdf
	 */
	static String dataFile = workingDir + "";

	static String exportQueryResultFile = "results" + File.separator+ "jenaQueriesResults" + File.separator + "export_jena_query_result.txt";

	static String outputCSVFile =  "outputJenaMetics.csv";

	static long readingDataTimeInMilliseconds;
	static long readingQueriesTimeInMilliseconds;
	static long evaluationQueriesTimeInMilliseconds;
	static long evaluationWorkLoadTimeInMilliseconds;
	static long totalTimeInMilliseconds;
	static long nbQueries;
	static double rendement;

	public static void main(String[] args) throws Exception {
		Timer timerTotal = new Timer();
		timerTotal.startTimer();

	
		final Options options = configParameters();
		final CommandLineParser parser = new DefaultParser();
		final CommandLine line = parser.parse(options, args);

		boolean helpMode = line.hasOption("help");// args.length == 0
		if (helpMode) {
			final HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("RDFEngine", options, true);
			System.exit(0);
		}

		Timer timerReadData = new Timer();

		dataFile = line.getOptionValue("data", workingDir + "sample_data.nt");
		queryFile = line.getOptionValue("queries", workingDir + "sample_query.queryset");
	

		Model model = ModelFactory.createDefaultModel();
		// lecture des données
		InputStream in = RDFDataMgr.open(dataFile);
		if (in == null) {
			throw new IllegalArgumentException("File: " + dataFile + " not found");
		}
		timerReadData.startTimer();
		model.read(in, null, "N-TRIPLES");
		timerReadData.endTimer();
		readingDataTimeInMilliseconds = timerReadData.getTimeMilliseconds();
		// model.write(System.out);
		// lecture des requêttes
		JenaParseQueriesProcessor queryParser = new JenaParseQueriesProcessor();
		List<Query> queries = queryParser.parseQueries(queryFile);
		readingQueriesTimeInMilliseconds = queryParser.getTimerReadQueries().getTimeMilliseconds();
		nbQueries = queries.size();
		// traitement des requêtes
		StringBuilder queryResultSb = new StringBuilder();
		String tempTextResult = "";
		Timer evaluationQueriesTimer = new Timer();
		evaluationQueriesTimer.startTimer();
		evaluationQueriesTimer.pauseTimer();
		for (Query query : queries) {
			evaluationQueriesTimer.resumeTimer();
			QueryExecution execution = QueryExecutionFactory.create(query, model);
			try {
				ResultSet rs = execution.execSelect();
				evaluationQueriesTimer.pauseTimer();
				queryResultSb.append(query.toString());
				tempTextResult = ResultSetFormatter.asText(rs);
				queryResultSb.append(tempTextResult);

			} finally {
				execution.close();
			}
		}
		evaluationQueriesTimer.endTimer();
		evaluationQueriesTimeInMilliseconds = evaluationQueriesTimer.getTimeMilliseconds();
		Utility.createQueriesResultsDirectoy("jenaQueriesResults");
		Utility.outputText(exportQueryResultFile, queryResultSb.toString());
		
		
		evaluationWorkLoadTimeInMilliseconds = readingDataTimeInMilliseconds + readingQueriesTimeInMilliseconds
				+ evaluationQueriesTimeInMilliseconds;
		rendement = nbQueries / (readingQueriesTimeInMilliseconds + evaluationQueriesTimeInMilliseconds);
		rendement = rendement * 1000;
		timerTotal.endTimer();
		totalTimeInMilliseconds = timerTotal.getTimeMilliseconds();

		outputTimesCSV(outputCSVFile);

	}

	private static Options configParameters() {

		final Option helpFileOption = Option.builder("h").longOpt("help").desc("affiche le menu d'aide").build();

		final Option queriesOption = Option.builder("queries").longOpt("queries").hasArg(true)
				.argName("/chemin/vers/dossier/requetes").desc("Le chemin vers les queries").required(false).build();

		final Option dataOption = Option.builder("data").longOpt("data").hasArg(true)
				.argName("/chemin/vers/fichier/donnees").desc("Le chemin vers les donnees").required(false).build();

		// Create the options list
		final Options options = new Options();
		options.addOption(queriesOption);
		options.addOption(dataOption);
		options.addOption(helpFileOption);

		return options;
	}

	// output des metriques au format csv

	private static void outputTimesCSV(String outputFile) {

		StringBuilder sb = new StringBuilder();
		String[] headers = { "nom du fichier de données", "nom du fichier des requêtes", "nombre de requêtes",
				"temps de lecture des données (ms)", "temps de lecture des requêtes (ms)",
				"temps total d'évaluation des requetes (ms)", "temps total d'évaluation du workload (ms)",
				"temps total (du début à la fin du programme) (ms)", "Rendement (requetes/seconde)" };
		sb.append(convertToCSV(headers));
		sb.append("\n");
		String[] values = { dataFile, queryFile, String.valueOf(nbQueries),
				String.valueOf(readingDataTimeInMilliseconds), String.valueOf(readingQueriesTimeInMilliseconds),
				String.valueOf(evaluationQueriesTimeInMilliseconds),
				String.valueOf(evaluationWorkLoadTimeInMilliseconds), String.valueOf(totalTimeInMilliseconds),
				String.valueOf(rendement) };
		sb.append(convertToCSV(values));
		File csvOutputFile = Utility.getJenaMetricFile(outputFile);
		try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
			pw.println(sb.toString());
		} catch (FileNotFoundException e) {
			System.out.println("Fichier CSV des temps d'éxécution non trouvé.");
		}
	}

	public static String convertToCSV(String[] data) {
		return Stream.of(data).collect(Collectors.joining(","));
	}

}
