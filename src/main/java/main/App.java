package main;

import java.io.File;
import java.time.Instant;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class App {

	public static void main(String[] args) throws Exception {
		final Options options = configParameters();
		final CommandLineParser parser = new DefaultParser();
		final CommandLine line = parser.parse(options, args);

		boolean helpMode = line.hasOption("help");// args.length == 0
		if (helpMode) {
			final HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("RDFEngine", options, true);
			System.exit(0);
		}

		String switchMode = line.getOptionValue("switch", "0");
		String dataMode = line.getOptionValue("data", "");
		String queriesMode = line.getOptionValue("queries", "");
		String sizeOptimizedQuerysetMode = line.getOptionValue("size-optimized-queryset", "");
		String reponsesQEngineMode = line.getOptionValue("responses-qengine", "");
		String reponsesJenaEngineMode = line.getOptionValue("responses-jena", "");
		String resultsMode = line.getOptionValue("results", "");

		boolean missingArgs = true;
		switch (switchMode) {
		case "0": {
			System.out.println("Vous n'avez pas definis l'option d'execution **switch** !");
			break;
		}
		case "1": {
			missingArgs = dataMode.equals("") || queriesMode.equals("");
			if (missingArgs) {
				System.out.println("Arguments d'execution manquant pour ce mode...");
			} else {
				System.out.println("Execution du moteur...");
				String[] newArgs = new String[] { "--data", dataMode, "--queries", queriesMode };
				qengine.program.App.main(newArgs);
			}
			break;
		}
		case "2": {
			missingArgs = dataMode.equals("") || queriesMode.equals("");
			if (missingArgs) {
				System.out.println("Arguments d'execution manquant pour ce mode...");
			} else {
				System.out.println("Execution de JENA...");
				String[] newArgs = new String[] { "--data", dataMode, "--queries", queriesMode };
				qengineWithJena.App.main(newArgs);
			}
			break;
		}
		case "3": {
			missingArgs = dataMode.equals("") || queriesMode.equals("") || sizeOptimizedQuerysetMode.equals("");
			if (missingArgs) {
				System.out.println("Arguments d'execution manquant pour ce mode...");
			} else {
				System.out.println("Execution de la generation du queryset optimisé...");
				String[] newArgs = new String[] { "--data", dataMode, "--queries", queriesMode,
						"--size-optimized-queryset", sizeOptimizedQuerysetMode };
				benchmarkGenerator.App.main(newArgs);
			}
			break;
		}
		case "4": {
			missingArgs = reponsesQEngineMode.equals("") || reponsesJenaEngineMode.equals("");
			if (missingArgs) {
				System.out.println("Arguments d'execution manquant pour ce mode...");
			} else {
				System.out.println("Execution de la vérification de la complétude et de la correction...");
				String[] newArgs = new String[] { "--engine1", reponsesQEngineMode, "--jenaEngine",
						reponsesJenaEngineMode };
				engineComparator.App.main(newArgs);
			}
			break;
		}
		case "5": {
			missingArgs = resultsMode.equals("");
			if (missingArgs) {
				System.out.println("Arguments d'execution manquant pour ce mode...");
			} else {
				System.out.println("Execution du calcul de la moyenne robuste...");
				String[] newArgs = new String[] { "-data", resultsMode };
				metrics.App.main(newArgs);
			}
			break;
		}
		default:
			System.out.println("Cas d'execution non reconnu, vérifier la valeur de l'option **switch** !");
			break;
		}
	}

	private static Options configParameters() {

		final Option helpFileOption = Option.builder("h").longOpt("help").desc("Affiche le menu d'aide.").build();

		final Option switchOption = Option.builder("s").longOpt("switch").hasArg(true).argName("aNumber")
				.desc("1 : execute notre moteur." + "2 : execute jena."
						+ "3 : generation d'un fichier de requete optimiser."
						+ "4 : verification de la completude et de la correction.")
				.required(false).build();

		final Option dataOption = Option.builder("d").longOpt("data").hasArg(true)
				.argName("/chemin/vers/fichier/donnee.nt")
				.desc("Le chemin vers le fichier de donnee au format N-Triples.").required(false).build();

		final Option queriesOption = Option.builder("q").longOpt("queries").hasArg(true)
				.argName("/chemin/vers/fichier/requete.queryset")
				.desc("Le chemin vers le fichier de requete au format queryset.").required(false).build();

		final Option sizeOptimizedQuerysetOption = Option.builder("so").longOpt("size-optimized-queryset").hasArg(true)
				.argName("/chemin/vers/fichier/requete.queryset")
				.desc("Le chemin vers le fichier de requete optimisé (10% de requetes vide, 25% de doublon, 15% avec deux trois fois la meme requetes et 10% avec quatre fois la meme requetes) au format queryset a sauvegarder.")
				.required(false).build();

		final Option reponsesQEngineOption = Option.builder("rq").longOpt("responses-qengine").hasArg(true)
				.argName("/chemin/vers/fichier/reponses.txt")
				.desc("Le chemin vers le fichier contenant les requetes et leur réponses de notre moteur SPARQL dans fichier texte.")
				.required(false).build();

		final Option reponsesJenaEngineOption = Option.builder("rj").longOpt("responses-jena").hasArg(true)
				.argName("/chemin/vers/fichier/reponses.txt")
				.desc("Le chemin vers le fichier contenant les requetes et leur réponses de JENA dans fichier texte.")
				.required(false).build();

		final Option resultsOption = Option.builder("r").longOpt("results").hasArg(true)
				.argName("/chemin/vers/dossier/results")
				.desc("Le chemin vers le dossier contenant les resultats du benchmark au format queryset.")
				.required(false).build();

		// Create the options list
		final Options options = new Options();
		options.addOption(helpFileOption);
		options.addOption(switchOption);
		options.addOption(dataOption);
		options.addOption(queriesOption);
		options.addOption(sizeOptimizedQuerysetOption);
		options.addOption(reponsesQEngineOption);
		options.addOption(reponsesJenaEngineOption);
		options.addOption(resultsOption);

		return options;
	}

}