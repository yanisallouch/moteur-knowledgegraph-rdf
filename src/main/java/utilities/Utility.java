package utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.rdf4j.query.parser.ParsedQuery;

import dictionary.Dictionary;
import queries.QueryWithDuplicates;
import timer.Timer;

public class Utility {

	private static StringBuilder sb = new StringBuilder();

	/*
	 * affichage des solution de la requête
	 */
	public static void displayQueriesSolutions(List<Long> solIndexes, Timer timerSingleQuery, Dictionary dictionary) {
		System.out.println("\n/******************************Solution (s) ********************/ \n");
		System.out.println("Temps d'évaluation en milliseconds: " + timerSingleQuery.getTimeMilliseconds());
		if (solIndexes.isEmpty()) {
			System.out.println("Pas de solutions ");
		} else {
			for (Long ind : solIndexes) {
				System.out.println(dictionary.getElementOfIndex(ind));
			}
		}
	}

	public static void outputSolutions(String outputFile) {
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(outputFile));
			writer.write(sb.toString());
			writer.close();
		} catch (IOException e) {
		}
		sb.append("\n");
	}

	public static void addSolutionsToPrinter(ParsedQuery query, Timer timerSingleQuery, Set<Long> solIndexes,
			Dictionary dictionary) {
		sb.append(query.getSourceString().trim());
		sb.append("\n");
		sb.append("Temps d'évaluation en milliseconds: " + timerSingleQuery.getTimeMilliseconds());
		sb.append("\n");
		if (!solIndexes.isEmpty()) {
			sb.append("Solution(s) :");
			sb.append("\n");
			for (Long ind : solIndexes) {
				sb.append(dictionary.getElementOfIndex(ind));
				sb.append("\n");
			}
		} else {
			sb.append("Pas de solution(s)");
			sb.append("\n");
		}
		sb.append("\n");
	}

	public static void outPutNbQueriesWithZeroSolutions(String outputFile, long nb) {
		BufferedWriter writer;
		try {
			FileWriter fw = new FileWriter(outputFile, false);
			writer = new BufferedWriter(fw);
			writer.write("Le nombre de requêtes avec zéro solutions est : " + nb);
			writer.write("\n");
			writer.write("\n");
			writer.close();
			fw.close();
		} catch (IOException e) {
		}
	}

	public static void outPutNbQueriesWithSameNbConditions(String outputFile,
			Map<Integer, List<ParsedQuery>> queriesWithPaterns) {
		BufferedWriter writer;
		try {
			FileWriter fw = new FileWriter(outputFile, true);
			writer = new BufferedWriter(fw);
			writer.write("Le nombre de requêtes avec le même nombre de conditions est : " + queriesWithPaterns.size());
			writer.write("\n");
			for (Integer nbPatern : queriesWithPaterns.keySet()) {
				writer.write("\t Le nombre de requêtes avec  " + nbPatern + " conditions est : "
						+ queriesWithPaterns.get(nbPatern).size());
				writer.write("\n");
			}
			writer.write("\n");
			writer.write("\n");
			writer.close();
			fw.close();
		} catch (IOException e) {
		}
	}

	public static void outPutNbQueriesWithDuplicates(String outputFile,
			List<QueryWithDuplicates> queriesWithDuplicates) {
		BufferedWriter writer;
		int cpt = 0;
		try {
			FileWriter fw = new FileWriter(outputFile, true);
			writer = new BufferedWriter(fw);
			writer.write("Liste des requêtes avec leurs nombre de doublons : ");
			writer.write("\n");
			for (QueryWithDuplicates qwd : queriesWithDuplicates) {
				writer.write("\n");
				writer.write("\t requête : " + qwd.getQuery() + "\n nombre de doublons : " + qwd.getNbDuplicates());
				writer.write("\n");
				cpt = cpt + qwd.getNbDuplicates();
			}
			writer.write("\n");
			writer.write("Le nombre totat de doublons dans les requêtes : " + cpt);
			writer.write("\n");
			writer.close();
			fw.close();
		} catch (IOException e) {
		}
	}

	public static void outputText(String outputFile, String text) {
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(outputFile));
			writer.write(text);
			writer.close();
		} catch (IOException e) {
		}
	}

	public static List<String> readTxtFile(String fileName) {
		List<String> lines = new LinkedList<String>();
		BufferedReader objReader = null;
		try {
			String strCurrentLine;

			objReader = new BufferedReader(new FileReader(fileName));

			while ((strCurrentLine = objReader.readLine()) != null) {
				lines.add(strCurrentLine.trim());
			}

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {
				if (objReader != null)
					objReader.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return lines;
	}

	public static File getJenaMetricFile(String fileName) {

		File rootFolder = new File("results");
		if (!rootFolder.exists()) {
			rootFolder.mkdir();
		}
		String subRootFolderName = "results"+File.separator+"JenaMetricsResults";
		File subRootFolder = new File(subRootFolderName);
		if(!subRootFolder.exists()) {
			subRootFolder.mkdir();
		}
		final long timestamp = Instant.now().getEpochSecond();
		String outputFolderName = subRootFolderName + File.separator + "metricResult" + timestamp;
		File outputFolder = new File(outputFolderName);
		outputFolder.mkdir();
		String fn = outputFolderName + File.separator + fileName;
		File f = new File(fn);
		return f;
	}
	
	public static File getQEngineMetricFile(String fileName) {

		File rootFolder = new File("results");
		if (!rootFolder.exists()) {
			rootFolder.mkdir();
		}
		String subRootFolderName = "results"+File.separator+"qEngineMetricsResults";
		File subRootFolder = new File(subRootFolderName);
		if(!subRootFolder.exists()) {
			subRootFolder.mkdir();
		}
		final long timestamp = Instant.now().getEpochSecond();
		String outputFolderName = subRootFolderName + File.separator + "metricResult" + timestamp;
		File outputFolder = new File(outputFolderName);
		outputFolder.mkdir();
		String fn = outputFolderName + File.separator + fileName;
		File f = new File(fn);
		return f;
	}

	public static void createQueriesResultsDirectoy(String subFolderName) {
		File rootFolder = new File("results");
		if (!rootFolder.exists()) {
			rootFolder.mkdir();
	
		}
		String subRootFolderName = "results"+File.separator+subFolderName;
		File subRootFolder = new File(subRootFolderName);
		if(!subRootFolder.exists()) {
			subRootFolder.mkdir();
		}
		
	}

}
