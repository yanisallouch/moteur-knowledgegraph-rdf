package processors;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.rdf4j.query.algebra.Projection;
import org.eclipse.rdf4j.query.algebra.StatementPattern;
import org.eclipse.rdf4j.query.algebra.helpers.AbstractQueryModelVisitor;
import org.eclipse.rdf4j.query.algebra.helpers.StatementPatternCollector;
import org.eclipse.rdf4j.query.parser.ParsedQuery;

import comparator.QueryComparator;
import dictionary.Dictionary;
import index.OPSIndex;
import index.POSIndex;
import queries.QueryWithDuplicates;
import timer.Timer;
import utilities.Utility;

/**
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 */
public class QueryProcessor {
	Set<Long> solIndexes;
	Dictionary dictionary;
	OPSIndex opsIndex;
	POSIndex posIndex;
	Timer evaluationQueriesTimer;
	Timer timerSingleQuery;
	Timer timerReadQuery;
	long nbQueries = 0;
	private List<ParsedQuery> queries;
	private List<ParsedQuery> queriesWithEmptySolutions;
	Map<Integer, List<ParsedQuery>> queriesWithPaterns;
	List<QueryWithDuplicates> queriesWithDuplicates;

	public QueryProcessor(Dictionary dictionary, OPSIndex opsIndex, POSIndex posIndex) {
		super();
		this.dictionary = dictionary;
		this.opsIndex = opsIndex;
		this.posIndex = posIndex;
		this.evaluationQueriesTimer = new Timer();
		this.timerSingleQuery = new Timer();
		this.timerReadQuery = new Timer();
		queriesWithEmptySolutions = new LinkedList<ParsedQuery>();
		queriesWithPaterns = new HashMap();
		queriesWithDuplicates = new LinkedList<QueryWithDuplicates>();
	}

	public void parseAndProcessQueries(String queryFile, String baseURI, String outputFile)
			throws FileNotFoundException, IOException {

		ParseQueriesProcessor queryParser = new ParseQueriesProcessor();
		timerReadQuery.startTimer();
		queries = queryParser.parseQueries(queryFile, baseURI);
		nbQueries = queries.size();
		timerReadQuery.endTimer();

		evaluationQueriesTimer.startTimer();
		processAllQueries(queries);
		evaluationQueriesTimer.endTimer();

		Utility.outputSolutions(outputFile);
	}

	private void processAllQueries(List<ParsedQuery> queries2) throws IOException {
		for (ParsedQuery parsedQuery : queries2) {
			processAQuery2(parsedQuery);
		}

	}

	/**
	 * Méthode utilisée ici lors du parsing de requête sparql pour agir sur l'objet
	 * obtenu.
	 * 
	 * @throws IOException
	 */
	public void processAQuery2(ParsedQuery query) throws IOException {
		timerSingleQuery.startTimer();
		List<StatementPattern> patterns = StatementPatternCollector.process(query.getTupleExpr());
		// Utilisation d'une classe anonyme
		query.getTupleExpr().visit(new AbstractQueryModelVisitor<RuntimeException>() {
			public void meet(Projection projection) {
			}
		});

		// traitement de la requête
		solIndexes = new HashSet<Long>();
		boolean b = false;
		StatementPattern pattern;
		for (int i = 0; i < patterns.size(); i++) {
			pattern = patterns.get(i);
			if (i == 0) {
				b = resolveFirstPattern(pattern);
			} else {
				if (b) {
					b = resolveOtherPattern(pattern);
				}

			}
		}
		timerSingleQuery.endTimer();

		evaluationQueriesTimer.pauseTimer();

		Utility.addSolutionsToPrinter(query, timerSingleQuery, solIndexes, dictionary);

		// ajout de la query dans la map des patterns
		addQueryToQueryWithPatterns(query, patterns.size());

		// sauvegrader dans la liste des requêtes sans solution si elle n'a pas de sol

		if (solIndexes == null || solIndexes.isEmpty()) {
			queriesWithEmptySolutions.add(query);
		}

		// ajouter dans la map du nombre de doublons de chaque requête.
		addToQueriesWithDuplicates(query);

		evaluationQueriesTimer.resumeTimer();

	}

	private void addToQueriesWithDuplicates(ParsedQuery parsedQuery) {
		QueryComparator comparator = new QueryComparator();
		boolean b = false;
		for (QueryWithDuplicates qrwd : queriesWithDuplicates) {
			if (comparator.compare(qrwd.getQuery(), parsedQuery) == 0) {
				qrwd.incrementDuplications();
				b = true;
			}
		}
		if (!b) {
			QueryWithDuplicates q = new QueryWithDuplicates(parsedQuery);
			queriesWithDuplicates.add(q);
		}

	}

	private void addQueryToQueryWithPatterns(ParsedQuery query, int nbPaterns) {
		if (queriesWithPaterns.containsKey(nbPaterns)) {
			queriesWithPaterns.get(nbPaterns).add(query);
		} else {
			List<ParsedQuery> tempQueries = new LinkedList<ParsedQuery>();
			tempQueries.add(query);
			queriesWithPaterns.put(nbPaterns, tempQueries);
		}

	}

	private boolean resolveFirstPattern(StatementPattern pattern) {
		String Predicate = pattern.getPredicateVar().getValue().stringValue().trim();
		String object = pattern.getObjectVar().getValue().stringValue().trim();
		long indPredicate = dictionary.getIndexOfElement(Predicate);
		long indObject = dictionary.getIndexOfElement(object);
		boolean indiceNotExists = (Long.compare(indPredicate, -1) == 0) || (Long.compare(indObject, -1) == 0);
		if (indiceNotExists) {
			// pas de solutions
			return false;
		} else {
			solIndexes = posIndex.getListSubjects(indPredicate, indObject);
			
		}
		if (solIndexes == null) {
			solIndexes = new HashSet<Long>();
			return false;
		}
			
		
		return true;
	}

	private boolean resolveOtherPattern(StatementPattern pattern) {
		Set<Long> tempList = new HashSet<Long>();
		String Predicate = pattern.getPredicateVar().getValue().stringValue().trim();
		String object = pattern.getObjectVar().getValue().stringValue().trim();
		long indPredicate = dictionary.getIndexOfElement(Predicate);
		long indObject = dictionary.getIndexOfElement(object);
		if ((Long.compare(indPredicate, -1) == 0) || (Long.compare(indObject, -1) == 0)) {
			// pas de solutions
			solIndexes = new HashSet<Long>();
			return false;
		} else {
			tempList = posIndex.getListSubjects(indPredicate, indObject);
		}

		if (tempList != null && tempList.size() > 0) {
			solIndexes.retainAll(tempList);
			return true;
		} else {
			// pas de solution vu que ce pattern n'a aucun élément
			solIndexes = new HashSet<Long>();
			return false;
		}

	}

	public long getTimeEvaluationQueries() {
		return evaluationQueriesTimer.getTimeMilliseconds();
	}

	public long getTimeReadQuery() {
		return timerReadQuery.getTimeMilliseconds();
	}

	public long getNbQueries() {
		return nbQueries;
	}

	public List<ParsedQuery> getQueries() {
		return queries;
	}

	public List<ParsedQuery> getQueriesWithEmptySolutions() {
		return queriesWithEmptySolutions;
	}

	public Map<Integer, List<ParsedQuery>> getQueriesWithPaterns() {
		return queriesWithPaterns;
	}

	public List<QueryWithDuplicates> getQueriesWithDuplicates() {
		return queriesWithDuplicates;
	}

	public void setQueriesWithDuplicates(List<QueryWithDuplicates> queriesWithDuplicates) {
		this.queriesWithDuplicates = queriesWithDuplicates;
	}

}
