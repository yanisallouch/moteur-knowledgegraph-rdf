package processors;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import org.eclipse.rdf4j.query.MalformedQueryException;
import org.eclipse.rdf4j.query.parser.ParsedQuery;
import org.eclipse.rdf4j.query.parser.sparql.SPARQLParser;

public class ParseQueriesProcessor {

	

	public ParseQueriesProcessor() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Traite chaque requête lue dans {@link #queryFile} avec
	 * {@link #processAQuery(ParsedQuery)}.
	 */
	public List<ParsedQuery> parseQueries(String queryFile, String baseURI)
			throws FileNotFoundException, IOException {
		/**
		 * Try-with-resources
		 * 
		 * @see <a href=
		 *      "https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html">Try-with-resources</a>
		 */
		/*
		 * On utilise un stream pour lire les lignes une par une, sans avoir à toutes
		 * les stocker entièrement dans une collection.
		 */
		List<ParsedQuery>queries = new LinkedList<ParsedQuery>();
		try (Stream<String> lineStream = Files.lines(Paths.get(queryFile))) {
			SPARQLParser sparqlParser = new SPARQLParser();
			Iterator<String> lineIterator = lineStream.iterator();
			StringBuilder queryString = new StringBuilder();

			String line;
			ParsedQuery query;
			while (lineIterator.hasNext()) {
				/*
				 * On stocke plusieurs lignes jusqu'à ce que l'une d'entre elles se termine par
				 * un '}' On considère alors que c'est la fin d'une requête
				 */
				line = lineIterator.next();
				queryString.append(line);

				if (line.trim().endsWith("}")) {
						query = sparqlParser.parseQuery(queryString.toString(), baseURI);
						queries.add(query);
						queryString = new StringBuilder();
				}
			}
			
		}
		return queries;
	}

	
	
}
