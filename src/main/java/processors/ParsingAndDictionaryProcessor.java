package processors;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.Rio;

import dictionary.Dictionary;
import qengine.program.MainRDFHandler;
import resources.Resource;
import resources.TripletBase;
import timer.Timer;

/**
 * 
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 *
 */
public class ParsingAndDictionaryProcessor {

	Dictionary dictionary;
	TripletBase dbLoaded;
	Timer timerReadData = new Timer();
	Timer timerCreateDictionary = new Timer();

	/**
	 * Traite chaque triple lu dans {@link #dataFile} avec {@link MainRDFHandler}.
	 */
	private void parseData(String dataFile, String baseURI) throws FileNotFoundException, IOException {
		timerReadData.startTimer();
		try (Reader dataReader = new FileReader(dataFile)) {
			// On va parser des données au format ntriples
			RDFParser rdfParser = Rio.createParser(RDFFormat.NTRIPLES);

			// On utilise notre implémentation de handler
			rdfParser.setRDFHandler(new MainRDFHandler());

			// Parsing et traitement de chaque triple par le handler
			rdfParser.parse(dataReader, baseURI);
		}
		timerReadData.endTimer();
	}

	/**
	 * créartion du dictionaire
	 */
	private void createDictionary() {
		timerCreateDictionary.startTimer();
		this.dictionary = new Dictionary();
		long cpt = 0;
		for (Resource res : dbLoaded.getAllResources()) {
			if (!dictionary.containsElement(res.getSubject())) {
				dictionary.addElement(res.getSubject(), cpt);
				cpt++;
			}
			if (!dictionary.containsElement(res.getPredicate())) {
				dictionary.addElement(res.getPredicate(), cpt);
				cpt++;
			}
			if (!dictionary.containsElement(res.getObject())) {
				dictionary.addElement(res.getObject(), cpt);
				cpt++;
			}
		}
		timerCreateDictionary.endTimer();
	}

	/**
	 * Chargement de la base de triplets et création d’un dictionnaire pour les
	 * ressources.
	 * 
	 * @param dataFile
	 * @param baseURI
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void loadDataAndCreateDictionary(String dataFile, String baseURI) throws FileNotFoundException, IOException {
		this.parseData(dataFile, baseURI);
		dbLoaded = TripletBase.getInstance();
		this.createDictionary();
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public TripletBase getDbLoaded() {
		return dbLoaded;
	}

	public long getTimeReadData() {
		return timerReadData.getTimeMilliseconds();
	}

	public long getTimeCreateDictionary() {
		return timerCreateDictionary.getTimeMilliseconds();
	}
}
