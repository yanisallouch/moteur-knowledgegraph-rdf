package processors;

import dictionary.Dictionary;
import index.HexastoreIndex;
import index.OPSIndex;
import index.OSPIndex;
import index.POSIndex;
import index.PSOIndex;
import index.SOPIndex;
import index.SPOIndex;
import resources.Resource;
import resources.TripletBase;
import timer.Timer;

/**
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 */
public class IndexingProcessor {
	Timer timerCreateIndex;

	public IndexingProcessor() {
		super();
		timerCreateIndex = new Timer();
	}

	public HexastoreIndex createIndex(Dictionary dico, TripletBase dbLoaded, String type) {
		timerCreateIndex.startTimer();
		HexastoreIndex index = null;
		switch (type) {
		case "OPS": {
			index = new OPSIndex();
			break;
		}
		case "OSP": {
			index = new OSPIndex();
			break;
		}
		case "POS": {
			index = new POSIndex();
			break;
		}
		case "PSO": {
			index = new PSOIndex();
			break;
		}
		case "SOP": {
			index = new SOPIndex();
			break;
		}
		default: {
			index = new SPOIndex();
			break;
		}
		}

		long indSubject;
		long indPredicate;
		long indObject;
		for (Resource resource : dbLoaded.getAllResources()) {
			indSubject = dico.getIndexOfElement(resource.getSubject());
			indPredicate = dico.getIndexOfElement(resource.getPredicate());
			indObject = dico.getIndexOfElement(resource.getObject());
			index.addIndexElement(indSubject, indPredicate, indObject);
		}
		timerCreateIndex.endTimer();
		return index;
	}

	public long getTimeCreateIndex() {
		return timerCreateIndex.getTimeMilliseconds();
	}

}
