package qengine.program;

import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.rio.helpers.AbstractRDFHandler;

import resources.Resource;
import resources.TripletBase;

/**
 * Le RDFHandler intervient lors du parsing de données et permet d'appliquer un
 * traitement pour chaque élément lu par le parseur.
 * 
 * <p>
 * Ce qui servira surtout dans le programme est la méthode
 * {@link #handleStatement(Statement)} qui va permettre de traiter chaque triple
 * lu.
 * </p>
 * <p>
 * À adapter/réécrire selon vos traitements.
 * </p>
 * 
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 */
public final class MainRDFHandler extends AbstractRDFHandler {

	@Override
	public void handleStatement(Statement st) {
		TripletBase.getInstance().addResource(new Resource(st.getSubject().stringValue().trim(),
				st.getPredicate().stringValue().trim(), st.getObject().stringValue().trim()));
	};

}