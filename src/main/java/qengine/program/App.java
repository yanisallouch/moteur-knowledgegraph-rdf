package qengine.program;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import dictionary.Dictionary;
import index.HexastoreIndex;
import index.OPSIndex;
import index.POSIndex;
import processors.IndexingProcessor;
import processors.ParsingAndDictionaryProcessor;
import processors.QueryProcessor;
import resources.TripletBase;
import timer.Timer;
import utilities.Utility;

/**
 * Programme simple lisant un fichier de requête et un fichier de données.
 * 
 * <p>
 * Les entrées sont données ici de manière statique, à vous de programmer les
 * entrées par passage d'arguments en ligne de commande comme demandé dans
 * l'énoncé.
 * </p>
 * 
 * <p>
 * Le présent programme se contente de vous montrer la voie pour lire les
 * triples et requêtes depuis les fichiers ; ce sera à vous d'adapter/réécrire
 * le code pour finalement utiliser les requêtes et interroger les données. On
 * ne s'attend pas forcémment à ce que vous gardiez la même structure de code,
 * vous pouvez tout réécrire.
 * </p>
 * 
 * @author Olivier Rodriguez <olivier.rodriguez1@umontpellier.fr>
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 */
public class App {
	static final String baseURI = null;
	static final String SOP = "SOP";
	static final String SPO = "SPO";
	static final String OPS = "OPS";
	static final String OSP = "OSP";
	static final String POS = "POS";
	static final String PSO = "PSO";

	/**
	 * Votre répertoire de travail où vont se trouver les fichiers à lire
	 */
	static String workingDir = "data" + File.separator;

	/**
	 * Fichier contenant les requêtes sparql
	 */
	static String queryFile = workingDir + "";

	/**
	 * Fichier contenant des données rdf
	 */
	static String dataFile = workingDir + "";

	static String outputCSVFile = "outPutQEngineMetrics.csv";

	static String outputBenchmarkInfoFile = "results" + File.separator + "benchmarkInfo" + File.separator
			+ "benchmarkInfo.txt";

	static String exportQueryResultFile = "results" + File.separator + "qEngineQueriesResults" + File.separator
			+ "export_query_result.txt";

	static Timer timerTotalExecution = new Timer();

	static long timeReadDataInMilliseconds;
	static long timeCreateDictionaryInMilliseconds;
	static long timeTotalCreateIndexesInMilliseconds = 0;
	static long timeIndexSPOInMilliseconds;
	static long timeIndexSOPInMilliseconds;
	static long timeIndexPSOInMilliseconds;
	static long timeIndexPOSInMilliseconds;
	static long timeIndexOPSInMilliseconds;
	static long timeIndexOSPInMilliseconds;
	static long timeReadQueryInMilliseconds;
	static long timeEvaluationQueriesInMilliseconds;
	static long timeEvaluationWorkLoadInMilliseconds;
	static long timeTotalExecutionInMilliseconds;
	static long nbTripletRDF = 0;
	static long nbQueries = 0;
	static long nbIndex = 0;
	static double rendement = 0;
	// ========================================================================

	/**
	 * Entrée du programme
	 */
	public static void main(String[] args) throws Exception {
		timerTotalExecution.startTimer();
		final Options options = configParameters();
		final CommandLineParser parser = new DefaultParser();
		final CommandLine line = parser.parse(options, args);

		boolean helpMode = line.hasOption("help");// args.length == 0
		if (helpMode) {
			final HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("RDFEngine", options, true);
			System.exit(0);
		}

		dataFile = line.getOptionValue("data", workingDir + "sample_data.nt");
		queryFile = line.getOptionValue("queries", workingDir + "sample_query.queryset");

		// création et affichage du Dictionaire
		ParsingAndDictionaryProcessor parsingAndDictionaryProcessor = new ParsingAndDictionaryProcessor();
		parsingAndDictionaryProcessor.loadDataAndCreateDictionary(dataFile, baseURI);
		timeReadDataInMilliseconds = parsingAndDictionaryProcessor.getTimeReadData();
		timeCreateDictionaryInMilliseconds = parsingAndDictionaryProcessor.getTimeCreateDictionary();
		TripletBase dbLoaded = parsingAndDictionaryProcessor.getDbLoaded();
		Dictionary dictionary = parsingAndDictionaryProcessor.getDictionary();
		nbTripletRDF = dictionary.getNbTripletRDF();

		// création de l'index SPO
		IndexingProcessor indexingProcessor = new IndexingProcessor();
		HexastoreIndex index;
		index = indexingProcessor.createIndex(dictionary, dbLoaded, SPO);
		timeIndexSPOInMilliseconds = indexingProcessor.getTimeCreateIndex();
		nbIndex++;
		index = indexingProcessor.createIndex(dictionary, dbLoaded, SOP);
		timeIndexSOPInMilliseconds = indexingProcessor.getTimeCreateIndex();
		nbIndex++;
		timeIndexPSOInMilliseconds = indexingProcessor.getTimeCreateIndex();
		nbIndex++;
		POSIndex posIndex = (POSIndex) indexingProcessor.createIndex(dictionary, dbLoaded, POS);
		timeIndexPOSInMilliseconds = indexingProcessor.getTimeCreateIndex();
		nbIndex++;
		OPSIndex opsIndex = (OPSIndex) indexingProcessor.createIndex(dictionary, dbLoaded, OPS);
		timeIndexOPSInMilliseconds = indexingProcessor.getTimeCreateIndex();
		nbIndex++;
		index = indexingProcessor.createIndex(dictionary, dbLoaded, OSP);
		timeIndexOSPInMilliseconds = indexingProcessor.getTimeCreateIndex();
		nbIndex++;
		timeTotalCreateIndexesInMilliseconds = timeIndexOPSInMilliseconds + timeIndexOSPInMilliseconds
				+ timeIndexPOSInMilliseconds + timeIndexPSOInMilliseconds + timeIndexSOPInMilliseconds
				+ timeIndexSPOInMilliseconds;

		QueryProcessor queryProcesor = new QueryProcessor(dictionary, opsIndex, posIndex);
		Utility.createQueriesResultsDirectoy("qEngineQueriesResults");
		queryProcesor.parseAndProcessQueries(queryFile, baseURI, exportQueryResultFile);
		nbQueries = queryProcesor.getNbQueries();
		timeReadQueryInMilliseconds = queryProcesor.getTimeReadQuery();
		timeEvaluationQueriesInMilliseconds = queryProcesor.getTimeEvaluationQueries();

		timeEvaluationWorkLoadInMilliseconds = timeReadDataInMilliseconds + timeCreateDictionaryInMilliseconds
				+ timeTotalCreateIndexesInMilliseconds + timeReadQueryInMilliseconds
				+ timeEvaluationQueriesInMilliseconds;

		rendement = nbQueries / (timeReadQueryInMilliseconds + timeEvaluationQueriesInMilliseconds);
		rendement = rendement * 1000;

		Utility.createQueriesResultsDirectoy("benchmarkInfo");
		Utility.outPutNbQueriesWithZeroSolutions(outputBenchmarkInfoFile,
				queryProcesor.getQueriesWithEmptySolutions().size());
		Utility.outPutNbQueriesWithSameNbConditions(outputBenchmarkInfoFile, queryProcesor.getQueriesWithPaterns());
		Utility.outPutNbQueriesWithDuplicates(outputBenchmarkInfoFile, queryProcesor.getQueriesWithDuplicates());
		timerTotalExecution.endTimer();
		timeTotalExecutionInMilliseconds = getTimeTotalExecution();
		outputTimesCSV(outputCSVFile);
	}

	// ========================================================================
	// Add options here
	private static Options configParameters() {

		final Option helpFileOption = Option.builder("h").longOpt("help").desc("affiche le menu d'aide").build();

		final Option queriesOption = Option.builder("queries").longOpt("queries").hasArg(true)
				.argName("/chemin/vers/dossier/requetes").desc("Le chemin vers les queries").required(false).build();

		final Option dataOption = Option.builder("data").longOpt("data").hasArg(true)
				.argName("/chemin/vers/fichier/donnees").desc("Le chemin vers les donnees").required(false).build();

		// Create the options list
		final Options options = new Options();
		options.addOption(queriesOption);
		options.addOption(dataOption);
		options.addOption(helpFileOption);

		return options;
	}

	private static long getTimeTotalExecution() {
		return timerTotalExecution.getTimeMilliseconds();
	}

	private static void outputTimesCSV(String outputFile) {
		StringBuilder sb = new StringBuilder();
		String[] headers = { "nom du fichier de données", "nom du dossier des requêtes", "nombre de triplets RDF",
				"nombre de requêtes", "temps de lecture des données (ms)", "temps de lecture des requêtes (ms)",
				"temps création dico (ms)", "nombre d'index", "temps de création des index (ms)",
				"temps total d'évaluation des requetes (ms)", "temps total d'évaluation du workload (ms)",
				"temps total (du début à la fin du programme) (ms)", "Rendement" };
		sb.append(convertToCSV(headers));
		sb.append("\n");
		String[] values = { dataFile, queryFile, String.valueOf(nbTripletRDF), String.valueOf(nbQueries),
				String.valueOf(timeReadDataInMilliseconds), String.valueOf(timeReadQueryInMilliseconds),
				String.valueOf(timeCreateDictionaryInMilliseconds), String.valueOf(nbIndex),
				String.valueOf(timeTotalCreateIndexesInMilliseconds),
				String.valueOf(timeEvaluationQueriesInMilliseconds),
				String.valueOf(timeEvaluationWorkLoadInMilliseconds), String.valueOf(timeTotalExecutionInMilliseconds),
				String.valueOf(rendement) };
		sb.append(convertToCSV(values));
		File csvOutputFile = Utility.getQEngineMetricFile(outputFile);
		try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
			pw.println(sb.toString());
		} catch (FileNotFoundException e) {
			System.out.println("Fichier CSV des temps d'éxécution non trouvé.");
		}
	}

	public static String convertToCSV(String[] data) {
		return Stream.of(data).collect(Collectors.joining(","));
	}
}
