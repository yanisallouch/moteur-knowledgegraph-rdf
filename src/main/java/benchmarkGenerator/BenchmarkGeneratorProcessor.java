package benchmarkGenerator;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.rdf4j.query.parser.ParsedQuery;

import comparator.QueryComparator;
import processors.QueryProcessor;
import queries.QueryWithDuplicates;

public class BenchmarkGeneratorProcessor {

	public List<ParsedQuery> generateBenchmarck(QueryProcessor queryProcesor, float percentZeroResp,
			long benchmarkSize) {
		int nbSol = (int) (benchmarkSize * percentZeroResp);
		if (nbSol > queryProcesor.getQueriesWithEmptySolutions().size()) {
			System.out.println(
					"Génération du benchmark Impossible : le nombre de requêtes avec 0 solutions est inférieur à "
							+ nbSol);
			System.exit(-1);
		} else {
			List<ParsedQuery> queriesWithEmptySolutions = getQueriesWithEmptySolutions(queryProcesor, percentZeroResp,
					benchmarkSize);
			int nbSol2 = (int) ((benchmarkSize * 20) / 100);
			int nbRes = (int) (queryProcesor.getNbQueries() - queriesWithEmptySolutions.size());
			if (nbRes < nbSol2) {
				System.out.println(
						"Génération du benchmark Impossible : le nombre de requêtes distinc pour la génération de requête avec doublons est inférieur à "
								+ nbSol2);
				System.exit(-1);
			} else {
				List<ParsedQuery> queriesWithDuplicates = getQueriesWithDuplicates(queryProcesor,
						queriesWithEmptySolutions, benchmarkSize);
				int nbSol3 = (int) ((benchmarkSize * 40) / 100);
				int nbRes2 = (int) (queryProcesor.getNbQueries()
						- (queriesWithEmptySolutions.size() + queriesWithDuplicates.size()));
				if (nbSol3 < nbRes2) {
					System.out.println(
							"Génération du benchmark Impossible : le nombre de requêtes distinct pour la génération de requête unique est inférieur à "
									+ nbSol3);
					System.exit(-1);
				} else {
					List<ParsedQuery> queriesUnique = getUniqueQueries(queryProcesor, queriesWithEmptySolutions,
							queriesWithDuplicates, benchmarkSize);
					List<ParsedQuery> benchmark = new LinkedList<ParsedQuery>();
					benchmark.addAll(queriesWithEmptySolutions);
					benchmark.addAll(queriesWithDuplicates);
					benchmark.addAll(queriesUnique);
					return benchmark;

				}

			}
		}
		return null;
	}

	public List<ParsedQuery> getQueriesWithEmptySolutions(QueryProcessor queryProcesor, float percentzeroresp,
			long benchmarksize) {
		List<ParsedQuery> queriesWithZeroResp = new LinkedList<ParsedQuery>();
		List<ParsedQuery> allQueriesWithZeroSol = new LinkedList<ParsedQuery>(
				queryProcesor.getQueriesWithEmptySolutions());
		Collections.shuffle(allQueriesWithZeroSol);
		int nbResp = (int) (benchmarksize * percentzeroresp);
		for (int i = 0; i < nbResp; i++) {
			queriesWithZeroResp.add(allQueriesWithZeroSol.get(i));
		}
		return queriesWithZeroResp;
	}

	public List<ParsedQuery> getQueriesWithDuplicates(QueryProcessor queryProcesor,
			List<ParsedQuery> queriesWithEmptySol, long benchmarksize) {
		List<ParsedQuery> queriesWithDuplicates = new LinkedList<ParsedQuery>();

		int nbQueriesWithOneDup = (int) ((benchmarksize * 25) / 100);
		int nbQueriesWithTwoDup = (int) ((benchmarksize * 15) / 100);
		int nbQueriesWithThreeDup = (int) ((benchmarksize * 10) / 100);

		List<ParsedQuery> allQueries = new LinkedList(queryProcesor.getQueries());
		allQueries.removeAll(queriesWithEmptySol);
		Collections.shuffle(allQueries);

		Set<ParsedQuery> allQueriesSet = new HashSet<ParsedQuery>(allQueries);
		allQueries = new LinkedList(allQueriesSet);
		addQueriesWithOneDuplication(allQueries, nbQueriesWithOneDup, queriesWithDuplicates);
		addQueriesWithOTwoDuplication(allQueries, nbQueriesWithTwoDup, queriesWithDuplicates);
		addQueriesWithOThreeDuplication(allQueries, nbQueriesWithThreeDup, queriesWithDuplicates);
		return queriesWithDuplicates;
	}

	private void addQueriesWithOneDuplication(List<ParsedQuery> allQueries, int nbQueriesWithOneDup,
			List<ParsedQuery> queriesWithDuplicates) {

		int i = 0;
		boolean b = false;
		for (ParsedQuery query : allQueries) {
			if (!containsQuery(queriesWithDuplicates, query)) {
				queriesWithDuplicates.add(query);
				queriesWithDuplicates.add(query);
				i = i + 2;
				if (i >= nbQueriesWithOneDup) {
					allQueries.removeAll(queriesWithDuplicates);
					b = true;
					break;
				}
			}
		}
		if (!b) {
			System.err.println("Géneration des requêtes avec 1 doubons Impossible : Nombre de requêtes insufisant");
			System.exit(-1);
		}

	}

	private void addQueriesWithOTwoDuplication(List<ParsedQuery> allQueries, int nbQueriesWithTwoDup,
			List<ParsedQuery> queriesWithDuplicates) {

		boolean b = false;
		int i = 0;
		for (ParsedQuery query : allQueries) {
			if (!containsQuery(queriesWithDuplicates, query)) {
				queriesWithDuplicates.add(query);
				queriesWithDuplicates.add(query);
				queriesWithDuplicates.add(query);
				i = i + 3;
				if (i >= nbQueriesWithTwoDup) {
					allQueries.removeAll(queriesWithDuplicates);
					b = true;
					break;
				}
			}
		}
		if (!b) {
			System.err.println("Géneration des requêtes avec 2 doubons Impossible : Nombre de requêtes insufisant");
			System.exit(-1);
		}

	}

	private void addQueriesWithOThreeDuplication(List<ParsedQuery> allQueries, int nbQueriesWithThreeDup,
			List<ParsedQuery> queriesWithDuplicates) {

		boolean b = false;
		int i = 0;
		for (ParsedQuery query : allQueries) {
			if (!containsQuery(queriesWithDuplicates, query)) {
				queriesWithDuplicates.add(query);
				queriesWithDuplicates.add(query);
				queriesWithDuplicates.add(query);
				queriesWithDuplicates.add(query);
				i = i + 4;
				if (i >= nbQueriesWithThreeDup) {
					allQueries.removeAll(queriesWithDuplicates);
					b = true;
					break;
				}
			}
		}
		if (!b) {
			System.err.println("Géneration des requêtes avec 3 doubons Impossible : Nombre de requêtes insufisant");
			System.exit(-1);
		}

	}

	private boolean containsQuery(List<ParsedQuery> list, ParsedQuery query) {
		QueryComparator queryComparator = new QueryComparator();
		for (ParsedQuery q : list) {
			if (queryComparator.compare(q, query) == 0) {
				return true;
			}
		}
		return false;
	}

	private List<ParsedQuery> getUniqueQueries(QueryProcessor queryProcesor,
			List<ParsedQuery> queriesWithEmptySolutions, List<ParsedQuery> queriesWithDuplicates, long benchmarkSize) {
		List<ParsedQuery> uniqueQueries = new LinkedList<ParsedQuery>();

		List<ParsedQuery> allQueries = new LinkedList<ParsedQuery>(queryProcesor.getQueries());
		allQueries.removeAll(queriesWithEmptySolutions);
		allQueries.removeAll(queriesWithDuplicates);
		Collections.shuffle(allQueries);
		Set<ParsedQuery> allQueriesSet = new HashSet<ParsedQuery>(allQueries);

		int nbUniqueQueries = (int) ((benchmarkSize * 40) / 100);
		addUniqueQueries(allQueries, nbUniqueQueries, uniqueQueries);

		return uniqueQueries;
	}

	private void addUniqueQueries(List<ParsedQuery> allQueries, int nbUniqueQueries, List<ParsedQuery> uniqueQueries) {
		int i = 0;
		boolean b = false;
		for (ParsedQuery query : allQueries) {
			if (!containsQuery(uniqueQueries, query)) {
				uniqueQueries.add(query);
				i++;
				if (i >= nbUniqueQueries) {
					allQueries.removeAll(uniqueQueries);
					b = true;
					break;
				}
			}
		}
		if (!b) {
			System.err.println("Géneration des requêtes unique impossible : Nombre de requêtes insufisant");
			System.exit(-1);
		}

	}

}
