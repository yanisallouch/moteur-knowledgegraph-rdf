package benchmarkGenerator;

import java.io.File;
import java.time.Instant;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.eclipse.rdf4j.query.parser.ParsedQuery;

import dictionary.Dictionary;
import index.HexastoreIndex;
import index.OPSIndex;
import index.POSIndex;
import processors.IndexingProcessor;
import processors.ParsingAndDictionaryProcessor;
import processors.QueryProcessor;
import resources.TripletBase;
import utilities.Utility;

public class App {

	static final String baseURI = null;
	/**
	 * Votre répertoire de travail où vont se trouver les fichiers à lire
	 */
	static String workingDir = "data" + File.separator;

	/**
	 * Fichier contenant les requêtes sparql
	 */
	static String queryFile = workingDir + "";

	/**
	 * Fichier contenant des données rdf
	 */
	static String dataFile = workingDir + "";

	static final long timestamp = Instant.now().getEpochSecond();
	static String benchmarkFile = "results" + File.separator + "benchmarkGenerated" + File.separator + "benchmarkFile-"
			+ String.valueOf(timestamp) + ".queryset";

	static final String POS = "POS";
	static final String OPS = "OPS";

	static final float percentZeroResp = 0.1f;
	static long benchmarkSize;

	public static void main(String[] args) throws Exception {
		final Options options = configParameters();
		final CommandLineParser parser = new DefaultParser();
		final CommandLine line = parser.parse(options, args);

		boolean helpMode = line.hasOption("help");// args.length == 0
		if (helpMode) {
			final HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("RDFEngine", options, true);
			System.exit(0);
		}

		dataFile = line.getOptionValue("data", workingDir + "sample_data.nt");
		queryFile = line.getOptionValue("queries", workingDir + "sample_query.queryset");
		try {
			benchmarkSize = Long.valueOf(line.getOptionValue("size-optimized-queryset", "1200"));
		} catch (NumberFormatException e) {
			benchmarkSize = 1200;
		}

		// création et affichage du Dictionaire
		ParsingAndDictionaryProcessor parsingAndDictionaryProcessor = new ParsingAndDictionaryProcessor();
		parsingAndDictionaryProcessor.loadDataAndCreateDictionary(dataFile, baseURI);
		TripletBase dbLoaded = parsingAndDictionaryProcessor.getDbLoaded();
		Dictionary dictionary = parsingAndDictionaryProcessor.getDictionary();

		IndexingProcessor indexingProcessor = new IndexingProcessor();

		POSIndex posIndex = (POSIndex) indexingProcessor.createIndex(dictionary, dbLoaded, POS);
		OPSIndex opsIndex = (OPSIndex) indexingProcessor.createIndex(dictionary, dbLoaded, OPS);

		QueryProcessor queryProcesor = new QueryProcessor(dictionary, opsIndex, posIndex);
		Utility.createQueriesResultsDirectoy("benchmarkGenerated");
		queryProcesor.parseAndProcessQueries(queryFile, baseURI, benchmarkFile);

		BenchmarkGeneratorProcessor benchGenProcessor = new BenchmarkGeneratorProcessor();

		List<ParsedQuery> benchmark = benchGenProcessor.generateBenchmarck(queryProcesor, percentZeroResp,
				benchmarkSize);
		StringBuilder sb = new StringBuilder();
		String queryStr;
		String tab1[];
		for (ParsedQuery query : benchmark) {
			queryStr = query.getSourceString().trim();
			tab1 = queryStr.split("\\{");
			tab1[0] = tab1[0] + "{\n";
			tab1[1] = tab1[1].replaceAll(" \\.", " .\n");
			queryStr = tab1[0] + tab1[1];
			sb.append(queryStr);
			sb.append("\n");
			sb.append("\n");
		}
		Utility.outputText(benchmarkFile, sb.toString());
	}

	private static Options configParameters() {

		final Option helpFileOption = Option.builder("h").longOpt("help").desc("affiche le menu d'aide").build();

		final Option queriesOption = Option.builder("queries").longOpt("queries").hasArg(true)
				.argName("/chemin/vers/dossier/requetes brute").desc("Le chemin vers les queries brute").required(false)
				.build();

		final Option dataOption = Option.builder("data").longOpt("data").hasArg(true)
				.argName("/chemin/vers/fichier/donnees").desc("Le chemin vers les donnees").required(false).build();

		final Option sizeOptimizedQuerysetOption = Option.builder("so").longOpt("size-optimized-queryset").hasArg(true)
				.argName("/chemin/vers/fichier/requete.queryset")
				.desc("Le chemin vers le fichier de requete optimisé (10% de requetes vide, 25% de doublon, 15% avec deux trois fois la meme requetes et 10% avec quatre fois la meme requetes) au format queryset a sauvegarder.")
				.required(false).build();

		// Create the options list
		final Options options = new Options();
		options.addOption(queriesOption);
		options.addOption(dataOption);
		options.addOption(helpFileOption);
		options.addOption(sizeOptimizedQuerysetOption);

		return options;
	}
}
