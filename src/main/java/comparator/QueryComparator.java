package comparator;

import java.util.Comparator;
import java.util.List;

import org.eclipse.rdf4j.query.algebra.StatementPattern;
import org.eclipse.rdf4j.query.algebra.helpers.StatementPatternCollector;
import org.eclipse.rdf4j.query.parser.ParsedQuery;

public class QueryComparator implements Comparator<ParsedQuery> {

	@Override
	public int compare(ParsedQuery q1, ParsedQuery q2) {
		List<StatementPattern> q1Patterns = StatementPatternCollector.process(q1.getTupleExpr());
		List<StatementPattern> q2Patterns = StatementPatternCollector.process(q2.getTupleExpr());
		if (q1Patterns.size() == q2Patterns.size()) {
			for (StatementPattern stm : q1Patterns) {
				if (!q2Patterns.contains(stm)) {
					return -1;
				}
			}
		}else {
			return -1;
		}

		return 0;
	}
}
