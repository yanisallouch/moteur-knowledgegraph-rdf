package engineComparator;

import java.util.HashSet;
import java.util.Set;

public class ParsedElement {

	private Set<String> responses;
	
	public ParsedElement(Set<String> response) {
		super();
		this.responses = response;
	}

	public ParsedElement() {
		super();
		this.responses = new HashSet<String>();
	}

	public Set<String> getResponses() {
		return responses;
	}

	public void setResponses(Set<String> responses) {
		this.responses = responses;
	}

	@Override
	public String toString() {
		return "ParsedElement [responses=" + responses + "]";
	}

	


	
}
