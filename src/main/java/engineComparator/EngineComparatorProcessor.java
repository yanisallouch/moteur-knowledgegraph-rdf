package engineComparator;

import java.util.LinkedList;
import java.util.List;

public class EngineComparatorProcessor {

	public boolean verifyCompleteness(List<ParsedElement> ourEngineResults, List<ParsedElement> jenaEngineResults) {
		if (ourEngineResults.size() != jenaEngineResults.size()) {
			return false;
		} else {
			for (int i = 0; i < ourEngineResults.size(); i++) {
				if (ourEngineResults.get(i).getResponses().size() != jenaEngineResults.get(i).getResponses().size()) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean verifyCorrecteness(List<ParsedElement> ourEngineResults, List<ParsedElement> jenaEngineResults) {

		if (verifyCompleteness(ourEngineResults, jenaEngineResults)) {
			List<String> ourReps;
			List<String> jenaReps;
			for (int i = 0; i < ourEngineResults.size(); i++) {
				ourReps = new LinkedList<String>(ourEngineResults.get(i).getResponses());
				jenaReps = new LinkedList<String>(jenaEngineResults.get(i).getResponses());
				if (!(ourReps.containsAll(jenaReps) && jenaReps.containsAll(ourReps))) {
					return false;
				}
			}
		}

		return true;
	}

}
