package engineComparator;

import java.util.LinkedList;
import java.util.List;

import utilities.Utility;

public class ParserProcessor {

	public List<ParsedElement> parseOurEngineFile(String fileName) {
		List<String> lines = Utility.readTxtFile(fileName);
		List<ParsedElement> results = new LinkedList<ParsedElement>();
		ParsedElement tempParsedElement = null;
		boolean b = false;
		for (String line : lines) {
			if (!b && line.equals("Solution(s) :")) {
				b = true;
				tempParsedElement = new ParsedElement();
			}
			if (b && !line.isEmpty() && !line.equals("Solution(s) :") ) {
				tempParsedElement.getResponses().add(line);
			}
			if (b && line.isEmpty()) {
				b = false;
				results.add(tempParsedElement);
			}
			if(!b && line.equals("Pas de solution(s)")) {
				tempParsedElement = new ParsedElement();
				results.add(tempParsedElement);
			}
		}
		return results;
	}
	
	public List<ParsedElement> parseJenaEngineFile(String fileName) {
		List<String> lines = Utility.readTxtFile(fileName);
		List<ParsedElement> results = new LinkedList<ParsedElement>();
		ParsedElement tempParsedElement = null;
		String tempLine ="";
		boolean b = false;
		String tab1[];
		String tab2[];
		for (String line : lines) {
			if (!b && line.startsWith("=")) {
				b = true;
				tempParsedElement = new ParsedElement();
			}
			if (b && line.substring(0,3).equals("| <") ) {
				tab1 = line.split("<");
				tab2 = tab1[1].split(">");
				tempLine = tab2[0];
				tempParsedElement.getResponses().add(tempLine);
			}
			if (b && line.startsWith("-")) {
				b = false;
				results.add(tempParsedElement);
			}
		}
		return results;
	}
}
