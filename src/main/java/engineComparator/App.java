package engineComparator;

import java.io.File;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class App {



	/**
	 * Fichier contenant les requêtes et réponses du premier moteur
	 */
	static String engine1File = "results"+File.separator+"qEngineQueriesResults" +File.separator+"export_query_result.txt";
	/**
	 * Fichier contenant les requêtes et réponses du second moteur( jena dans notre
	 * cas)
	 */
	static String engine2File = "results" + File.separator+ "jenaQueriesResults" + File.separator + "export_jena_query_result.txt";

	public static void main(String[] args) throws Exception {
		final Options options = configParameters();
		final CommandLineParser parser = new DefaultParser();
		final CommandLine line = parser.parse(options, args);

		boolean helpMode = line.hasOption("help");// args.length == 0
		if (helpMode) {
			final HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("RDFEngine", options, true);
			System.exit(0);
		}

		engine1File = line.getOptionValue("engine1", "results"+File.separator+"qEngineQueriesResults" +File.separator+"export_query_result.txt");
		engine2File = line.getOptionValue("jenaEngine", "results" + File.separator+ "jenaQueriesResults" + File.separator + "export_jena_query_result.txt");
		ParserProcessor parserProcessor = new ParserProcessor();

		List<ParsedElement> parsedElements = parserProcessor.parseOurEngineFile(engine1File);
		/*
		 * for(ParsedElement p :parsedElements) { System.out.println(p.toString()); }
		 */
		List<ParsedElement> parsedElements2 = parserProcessor.parseJenaEngineFile(engine2File);
		/*
		 * for (ParsedElement p : parsedElements2) { System.out.println(p.toString()); }
		 */
		EngineComparatorProcessor engineComparatorProcessor = new EngineComparatorProcessor();
		boolean b = engineComparatorProcessor.verifyCompleteness(parsedElements, parsedElements2);
		System.out.println(b);
		boolean b2 = engineComparatorProcessor.verifyCorrecteness(parsedElements, parsedElements2);
		System.out.println(b2);

	}

	private static Options configParameters() {

		final Option helpFileOption = Option.builder("h").longOpt("help").desc("affiche le menu d'aide").build();

		final Option engine1Option = Option.builder("engine1").longOpt("engine1").hasArg(true)
				.argName("/chemin/vers/dossier/moteur1").desc("Le chemin vers les infos du moteur1").required(false)
				.build();

		final Option jenaEngineOption = Option.builder("jenaEngine").longOpt("jeanEngine").hasArg(true)
				.argName("/chemin/vers/fichier/moteurJena").desc("Le chemin vers les infos du moteur Jena")
				.required(false).build();

		// Create the options list
		final Options options = new Options();
		options.addOption(engine1Option);
		options.addOption(jenaEngineOption);
		options.addOption(helpFileOption);

		return options;
	}
}
