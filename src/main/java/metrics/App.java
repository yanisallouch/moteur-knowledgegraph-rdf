package metrics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import utilities.Utility;

public class App {

	/**
	 * Fichier contenant des données rdf
	 */
	static String dataFolder = "";
	static String exportJenaMetricsAvgFile = "results" + File.separator + "jenaMetricsAvg" + File.separator
			+ "jenaMetricsAvg.csv";
	static String exportQEngineMetricsAvgFile = "results" + File.separator + "QEngineMetricsAvg" + File.separator
			+ "QEngineMetricsAvg.csv";

	public static void main(String[] args) throws Exception {
		final Options options = configParameters();
		final CommandLineParser parser = new DefaultParser();
		final CommandLine line = parser.parse(options, args);
		boolean helpMode = line.hasOption("help");// args.length == 0
		if (helpMode) {
			final HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("RDFEngine", options, true);
			System.exit(0);
		}
		dataFolder = line.getOptionValue("data", "results" + File.separator + "qEngineMetricsResults");
		boolean isJenaFolder = false;
		if (dataFolder.contains("JenaMetricsResults")) {
			isJenaFolder = true;
		}
		ParsingMetricFileProcessor parsingMetricFileProcessor = new ParsingMetricFileProcessor();
		MetricsProcessor metricsProcessor = new MetricsProcessor();

		File directory = new File(dataFolder);
		parsingMetricFileProcessor.readDirectory(directory, isJenaFolder);
		if (isJenaFolder) {
			List<JenaMetric> jenaMetrics = parsingMetricFileProcessor.getJenaMetrics();
			JenaMetricsAvg jenaMetricsAvg = metricsProcessor.getJenaMetricAvg(jenaMetrics);
			outputTimesCSV(exportJenaMetricsAvgFile, jenaMetricsAvg);
		} else {
			List<QEngineMetrics> qEngineMetrics = parsingMetricFileProcessor.getqEngineMetrics();
			QEngineMetricsAvg qEngineMetricsAvg = metricsProcessor.getQEngineMetricsAvg(qEngineMetrics);
			outputTimesCSV(exportQEngineMetricsAvgFile, qEngineMetricsAvg);

		}

	}

	private static Options configParameters() {

		final Option helpFileOption = Option.builder("h").longOpt("help").desc("affiche le menu d'aide").build();

		final Option dataOption = Option.builder("data").longOpt("data").hasArg(true)
				.argName("/chemin/vers/dossier/donnees").desc("Le chemin vers les donnees").required(false).build();

		// Create the options list
		final Options options = new Options();
		options.addOption(dataOption);
		options.addOption(helpFileOption);

		return options;
	}

	private static void outputTimesCSV(String outputFile, JenaMetricsAvg jMAvg) {

		StringBuilder sb = new StringBuilder();
		String[] headers = { "nom du fichier de données", "nom du fichier des requêtes", "nombre de requêtes",
				"temps moyen de lecture des données (ms)", "temps moyen de lecture des requêtes (ms)",
				"temps moyen d'évaluation des requetes (ms)", "temps moyen d'évaluation du workload (ms)",
				"temps moyen (du début à la fin du programme) (ms)", "Rendement moyen (requetes/seconde)" };
		sb.append(convertToCSV(headers));
		sb.append("\n");
		String[] values = { jMAvg.getDataFileName(), jMAvg.getQueryFileName(), jMAvg.getNbQueries(),
				jMAvg.getReadingDataTimeInMilliseconds(), jMAvg.getReadingQueriesTimeInMilliseconds(),
				jMAvg.getEvaluationQueriesTimeInMilliseconds(), jMAvg.getEvaluationWorkLoadTimeInMilliseconds(),
				jMAvg.getTotalTimeInMilliseconds(), jMAvg.getRendement() };
		sb.append(convertToCSV(values));
		Utility.createQueriesResultsDirectoy("jenaMetricsAvg");
		File csvOutputFile = new File(outputFile);
		try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
			pw.println(sb.toString());
		} catch (FileNotFoundException e) {
			System.out.println("Fichier CSV des temps d'éxécution non trouvé.");
		}
	}

	public static String convertToCSV(String[] data) {
		return Stream.of(data).collect(Collectors.joining(","));
	}

	private static void outputTimesCSV(String outputFile, QEngineMetricsAvg qEMAvg) {
		StringBuilder sb = new StringBuilder();
		String[] headers = { "nom du fichier de données", "nom du dossier des requêtes", "nombre de requêtes",
				"temps moyen de lecture des données (ms)", "temps moyen de lecture des requêtes (ms)",
				"temps moyen d'évaluation des requetes (ms)", "temps moyen d'évaluation du workload (ms)",
				"temps moyen (du début à la fin du programme) (ms)", "Rendement moyen (requetes/seconde)" };
		sb.append(convertToCSV(headers));
		sb.append("\n");
		String[] values = { qEMAvg.getDataFile(), qEMAvg.getQueryFile(), qEMAvg.getNbQueries(),
				qEMAvg.getTimeReadDataInMilliseconds(), qEMAvg.getTimeReadQueryInMilliseconds(),
				qEMAvg.getTimeEvaluationQueriesInMilliseconds(), qEMAvg.getTimeEvaluationWorkLoadInMilliseconds(),
				qEMAvg.getTimeTotalExecutionInMilliseconds(), qEMAvg.getRendement() };
		sb.append(convertToCSV(values));
		Utility.createQueriesResultsDirectoy("QEngineMetricsAvg");
		File csvOutputFile = new File(outputFile);
		try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
			pw.println(sb.toString());
		} catch (FileNotFoundException e) {
			System.out.println("Fichier CSV des temps d'éxécution non trouvé.");
		}
	}
}
