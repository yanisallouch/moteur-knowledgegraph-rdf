package metrics;

public class JenaMetric {
	
	private String dataFileName;
	private String queryFileName;
	private long nbQueries;
	private long readingDataTimeInMilliseconds;
	private long readingQueriesTimeInMilliseconds;
	private long evaluationQueriesTimeInMilliseconds;
	private long evaluationWorkLoadTimeInMilliseconds;
	private long totalTimeInMilliseconds;
	private double rendement;
	
	public JenaMetric() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JenaMetric(String dataFileName, String queryFileName, long nbQueries, long readingDataTimeInMilliseconds,
			long readingQueriesTimeInMilliseconds, long evaluationQueriesTimeInMilliseconds,
			long evaluationWorkLoadTimeInMilliseconds, long totalTimeInMilliseconds, double rendement) {
		super();
		this.dataFileName = dataFileName;
		this.queryFileName = queryFileName;
		this.nbQueries = nbQueries;
		this.readingDataTimeInMilliseconds = readingDataTimeInMilliseconds;
		this.readingQueriesTimeInMilliseconds = readingQueriesTimeInMilliseconds;
		this.evaluationQueriesTimeInMilliseconds = evaluationQueriesTimeInMilliseconds;
		this.evaluationWorkLoadTimeInMilliseconds = evaluationWorkLoadTimeInMilliseconds;
		this.totalTimeInMilliseconds = totalTimeInMilliseconds;
		this.rendement = rendement;
	}



	public String getDataFileName() {
		return dataFileName;
	}

	public void setDataFileName(String dataFileName) {
		this.dataFileName = dataFileName;
	}

	public String getQueryFileName() {
		return queryFileName;
	}

	public void setQueryFileName(String queryFileName) {
		this.queryFileName = queryFileName;
	}

	public long getReadingDataTimeInMilliseconds() {
		return readingDataTimeInMilliseconds;
	}

	public void setReadingDataTimeInMilliseconds(long readingDataTimeInMilliseconds) {
		this.readingDataTimeInMilliseconds = readingDataTimeInMilliseconds;
	}

	public long getReadingQueriesTimeInMilliseconds() {
		return readingQueriesTimeInMilliseconds;
	}

	public void setReadingQueriesTimeInMilliseconds(long readingQueriesTimeInMilliseconds) {
		this.readingQueriesTimeInMilliseconds = readingQueriesTimeInMilliseconds;
	}

	public long getEvaluationQueriesTimeInMilliseconds() {
		return evaluationQueriesTimeInMilliseconds;
	}

	public void setEvaluationQueriesTimeInMilliseconds(long evaluationQueriesTimeInMilliseconds) {
		this.evaluationQueriesTimeInMilliseconds = evaluationQueriesTimeInMilliseconds;
	}

	public long getEvaluationWorkLoadTimeInMilliseconds() {
		return evaluationWorkLoadTimeInMilliseconds;
	}

	public void setEvaluationWorkLoadTimeInMilliseconds(long evaluationWorkLoadTimeInMilliseconds) {
		this.evaluationWorkLoadTimeInMilliseconds = evaluationWorkLoadTimeInMilliseconds;
	}

	public long getTotalTimeInMilliseconds() {
		return totalTimeInMilliseconds;
	}

	public void setTotalTimeInMilliseconds(long totalTimeInMilliseconds) {
		this.totalTimeInMilliseconds = totalTimeInMilliseconds;
	}

	public long getNbQueries() {
		return nbQueries;
	}

	public void setNbQueries(long nbQueries) {
		this.nbQueries = nbQueries;
	}

	public double getRendement() {
		return rendement;
	}

	public void setRendement(double rendement) {
		this.rendement = rendement;
	}

	@Override
	public String toString() {
		return "JenaMetric [dataFileName=" + dataFileName + ", queryFileName=" + queryFileName + ", nbQueries="
				+ nbQueries + ", readingDataTimeInMilliseconds=" + readingDataTimeInMilliseconds
				+ ", readingQueriesTimeInMilliseconds=" + readingQueriesTimeInMilliseconds
				+ ", evaluationQueriesTimeInMilliseconds=" + evaluationQueriesTimeInMilliseconds
				+ ", evaluationWorkLoadTimeInMilliseconds=" + evaluationWorkLoadTimeInMilliseconds
				+ ", totalTimeInMilliseconds=" + totalTimeInMilliseconds + ", rendement=" + rendement + "]";
	}

	
}
