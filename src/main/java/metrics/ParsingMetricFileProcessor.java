package metrics;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import utilities.Utility;

public class ParsingMetricFileProcessor {
	
	private List<JenaMetric> jenaMetrics;
	private List<QEngineMetrics> qEngineMetrics;
	
	public ParsingMetricFileProcessor() {
		super();
		jenaMetrics = new LinkedList<JenaMetric>();
		qEngineMetrics = new LinkedList<QEngineMetrics>();
	}
	public void readDirectory(File folder,boolean isJenaFolder) {
		String fileName;
		List<String> lignesFichier = null;
	

		for (File file : folder.listFiles()) {
			if (!file.isDirectory()) {
				if (this.isWantedFile(file)) {
					getMetricsFromFile(file,isJenaFolder);
				}
			} else {
				readDirectory(file,isJenaFolder);
			
			}
		}

	}
	
	private boolean isWantedFile (File file) {

		final String extensionFileWnated = ".csv";
		final int indEndFileName = file.getName().length();
		final int indExtensionFile = indEndFileName - extensionFileWnated.length();
		final String fileExtentionName = file.getName().substring(indExtensionFile , indEndFileName);

		return fileExtentionName.equals(extensionFileWnated);
	}
	
	
	private void getMetricsFromFile(File file,boolean isJenaFolder) {
	
		List<String> lines = Utility.readTxtFile(file.getAbsolutePath());
		int ind = lines.size() -1;
		String tab[]=lines.get(ind).split(",");
		if(isJenaFolder) {
			
			JenaMetric jenaMetric = new JenaMetric(tab[0], tab[1], Long.parseLong(tab[2]), Long.parseLong(tab[3]),Long.parseLong(tab[3]),Long.parseLong(tab[4]), Long.parseLong(tab[5]), Long.parseLong(tab[6]), Double.parseDouble(tab[7]));
			jenaMetrics.add(jenaMetric);
		}else {
			QEngineMetrics qEMetrics = new QEngineMetrics(tab[0], tab[1], tab[2], tab[3], Long.parseLong(tab[4]), Long.parseLong(tab[5]), Long.parseLong(tab[6]), tab[7],Long.parseLong(tab[8]),Long.parseLong(tab[9]),Long.parseLong(tab[10]), Long.parseLong(tab[11]), Double.parseDouble(tab[12]));
			qEngineMetrics.add(qEMetrics);
		}
	}
	public List<JenaMetric> getJenaMetrics() {
		return jenaMetrics;
	}
	public List<QEngineMetrics> getqEngineMetrics() {
		return qEngineMetrics;
	}
	
	

	

}
