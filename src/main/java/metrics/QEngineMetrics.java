package metrics;

public class QEngineMetrics {

	private String dataFile;
	private String queryFile;
	private String nbTripletRDF;
	private String nbQueries;
	private long timeReadDataInMilliseconds;
	private long timeReadQueryInMilliseconds;
	private long timeCreateDictionaryInMilliseconds;
	private String nbIndex;
	private long timeTotalCreateIndexesInMilliseconds;
	private long timeEvaluationQueriesInMilliseconds;
	private long timeEvaluationWorkLoadInMilliseconds;
	private long timeTotalExecutionInMilliseconds;
	private double rendement;
	
	public QEngineMetrics() {
		super();
		// TODO Auto-generated constructor stub
	}

	public QEngineMetrics(String dataFile, String queryFile, String nbTripletRDF, String nbQueries,
			long timeReadDataInMilliseconds, long timeReadQueryInMilliseconds, long timeCreateDictionaryInMilliseconds,
			String nbIndex, long timeTotalCreateIndexesInMilliseconds, long timeEvaluationQueriesInMilliseconds,
			long timeEvaluationWorkLoadInMilliseconds, long timeTotalExecutionInMilliseconds, double rendement) {
		super();
		this.dataFile = dataFile;
		this.queryFile = queryFile;
		this.nbTripletRDF = nbTripletRDF;
		this.nbQueries = nbQueries;
		this.timeReadDataInMilliseconds = timeReadDataInMilliseconds;
		this.timeReadQueryInMilliseconds = timeReadQueryInMilliseconds;
		this.timeCreateDictionaryInMilliseconds = timeCreateDictionaryInMilliseconds;
		this.nbIndex = nbIndex;
		this.timeTotalCreateIndexesInMilliseconds = timeTotalCreateIndexesInMilliseconds;
		this.timeEvaluationQueriesInMilliseconds = timeEvaluationQueriesInMilliseconds;
		this.timeEvaluationWorkLoadInMilliseconds = timeEvaluationWorkLoadInMilliseconds;
		this.timeTotalExecutionInMilliseconds = timeTotalExecutionInMilliseconds;
		this.rendement = rendement;
	}

	public String getDataFile() {
		return dataFile;
	}

	public void setDataFile(String dataFile) {
		this.dataFile = dataFile;
	}

	public String getQueryFile() {
		return queryFile;
	}

	public void setQueryFile(String queryFile) {
		this.queryFile = queryFile;
	}

	public String getNbTripletRDF() {
		return nbTripletRDF;
	}

	public void setNbTripletRDF(String nbTripletRDF) {
		this.nbTripletRDF = nbTripletRDF;
	}

	public String getNbQueries() {
		return nbQueries;
	}

	public void setNbQueries(String nbQueries) {
		this.nbQueries = nbQueries;
	}

	public long getTimeReadDataInMilliseconds() {
		return timeReadDataInMilliseconds;
	}

	public void setTimeReadDataInMilliseconds(long timeReadDataInMilliseconds) {
		this.timeReadDataInMilliseconds = timeReadDataInMilliseconds;
	}

	public long getTimeReadQueryInMilliseconds() {
		return timeReadQueryInMilliseconds;
	}

	public void setTimeReadQueryInMilliseconds(long timeReadQueryInMilliseconds) {
		this.timeReadQueryInMilliseconds = timeReadQueryInMilliseconds;
	}

	public long getTimeCreateDictionaryInMilliseconds() {
		return timeCreateDictionaryInMilliseconds;
	}

	public void setTimeCreateDictionaryInMilliseconds(long timeCreateDictionaryInMilliseconds) {
		this.timeCreateDictionaryInMilliseconds = timeCreateDictionaryInMilliseconds;
	}

	public String getNbIndex() {
		return nbIndex;
	}

	public void setNbIndex(String nbIndex) {
		this.nbIndex = nbIndex;
	}

	public long getTimeTotalCreateIndexesInMilliseconds() {
		return timeTotalCreateIndexesInMilliseconds;
	}

	public void setTimeTotalCreateIndexesInMilliseconds(long timeTotalCreateIndexesInMilliseconds) {
		this.timeTotalCreateIndexesInMilliseconds = timeTotalCreateIndexesInMilliseconds;
	}

	public long getTimeEvaluationQueriesInMilliseconds() {
		return timeEvaluationQueriesInMilliseconds;
	}

	public void setTimeEvaluationQueriesInMilliseconds(long timeEvaluationQueriesInMilliseconds) {
		this.timeEvaluationQueriesInMilliseconds = timeEvaluationQueriesInMilliseconds;
	}

	public long getTimeEvaluationWorkLoadInMilliseconds() {
		return timeEvaluationWorkLoadInMilliseconds;
	}

	public void setTimeEvaluationWorkLoadInMilliseconds(long timeEvaluationWorkLoadInMilliseconds) {
		this.timeEvaluationWorkLoadInMilliseconds = timeEvaluationWorkLoadInMilliseconds;
	}

	public long getTimeTotalExecutionInMilliseconds() {
		return timeTotalExecutionInMilliseconds;
	}

	public void setTimeTotalExecutionInMilliseconds(long timeTotalExecutionInMilliseconds) {
		this.timeTotalExecutionInMilliseconds = timeTotalExecutionInMilliseconds;
	}

	public double getRendement() {
		return rendement;
	}

	public void setRendement(double rendement) {
		this.rendement = rendement;
	}

	@Override
	public String toString() {
		return "QEngineMetrics [dataFile=" + dataFile + ", queryFile=" + queryFile + ", nbTripletRDF=" + nbTripletRDF
				+ ", nbQueries=" + nbQueries + ", timeReadDataInMilliseconds=" + timeReadDataInMilliseconds
				+ ", timeReadQueryInMilliseconds=" + timeReadQueryInMilliseconds
				+ ", timeCreateDictionaryInMilliseconds=" + timeCreateDictionaryInMilliseconds + ", nbIndex=" + nbIndex
				+ ", timeTotalCreateIndexesInMilliseconds=" + timeTotalCreateIndexesInMilliseconds
				+ ", timeEvaluationQueriesInMilliseconds=" + timeEvaluationQueriesInMilliseconds
				+ ", timeEvaluationWorkLoadInMilliseconds=" + timeEvaluationWorkLoadInMilliseconds
				+ ", timeTotalExecutionInMilliseconds=" + timeTotalExecutionInMilliseconds + ", rendement=" + rendement
				+ "]";
	}
	
	

}
