package metrics;

public class JenaMetricsAvg {
	private String dataFileName;
	private String queryFileName;
	private String nbQueries;
	private String readingDataTimeInMilliseconds;
	private String readingQueriesTimeInMilliseconds;
	private String evaluationQueriesTimeInMilliseconds;
	private String evaluationWorkLoadTimeInMilliseconds;
	private String totalTimeInMilliseconds;
	private String rendement;
	
	public JenaMetricsAvg() {
		super();
		// TODO Auto-generated constructor stub
	}
	public JenaMetricsAvg(String dataFileName, String queryFileName, String nbQueries,
			String readingDataTimeInMilliseconds, String readingQueriesTimeInMilliseconds,
			String evaluationQueriesTimeInMilliseconds, String evaluationWorkLoadTimeInMilliseconds,
			String totalTimeInMilliseconds, String rendement) {
		super();
		this.dataFileName = dataFileName;
		this.queryFileName = queryFileName;
		this.nbQueries = nbQueries;
		this.readingDataTimeInMilliseconds = readingDataTimeInMilliseconds;
		this.readingQueriesTimeInMilliseconds = readingQueriesTimeInMilliseconds;
		this.evaluationQueriesTimeInMilliseconds = evaluationQueriesTimeInMilliseconds;
		this.evaluationWorkLoadTimeInMilliseconds = evaluationWorkLoadTimeInMilliseconds;
		this.totalTimeInMilliseconds = totalTimeInMilliseconds;
		this.rendement = rendement;
	}
	public String getDataFileName() {
		return dataFileName;
	}
	public void setDataFileName(String dataFileName) {
		this.dataFileName = dataFileName;
	}
	public String getQueryFileName() {
		return queryFileName;
	}
	public void setQueryFileName(String queryFileName) {
		this.queryFileName = queryFileName;
	}
	public String getNbQueries() {
		return nbQueries;
	}
	public void setNbQueries(String nbQueries) {
		this.nbQueries = nbQueries;
	}
	public String getReadingDataTimeInMilliseconds() {
		return readingDataTimeInMilliseconds;
	}
	public void setReadingDataTimeInMilliseconds(String readingDataTimeInMilliseconds) {
		this.readingDataTimeInMilliseconds = readingDataTimeInMilliseconds;
	}
	public String getReadingQueriesTimeInMilliseconds() {
		return readingQueriesTimeInMilliseconds;
	}
	public void setReadingQueriesTimeInMilliseconds(String readingQueriesTimeInMilliseconds) {
		this.readingQueriesTimeInMilliseconds = readingQueriesTimeInMilliseconds;
	}
	public String getEvaluationQueriesTimeInMilliseconds() {
		return evaluationQueriesTimeInMilliseconds;
	}
	public void setEvaluationQueriesTimeInMilliseconds(String evaluationQueriesTimeInMilliseconds) {
		this.evaluationQueriesTimeInMilliseconds = evaluationQueriesTimeInMilliseconds;
	}
	public String getEvaluationWorkLoadTimeInMilliseconds() {
		return evaluationWorkLoadTimeInMilliseconds;
	}
	public void setEvaluationWorkLoadTimeInMilliseconds(String evaluationWorkLoadTimeInMilliseconds) {
		this.evaluationWorkLoadTimeInMilliseconds = evaluationWorkLoadTimeInMilliseconds;
	}
	public String getTotalTimeInMilliseconds() {
		return totalTimeInMilliseconds;
	}
	public void setTotalTimeInMilliseconds(String totalTimeInMilliseconds) {
		this.totalTimeInMilliseconds = totalTimeInMilliseconds;
	}
	public String getRendement() {
		return rendement;
	}
	public void setRendement(String rendement) {
		this.rendement = rendement;
	}
	@Override
	public String toString() {
		return "JenaMetricsAvg [dataFileName=" + dataFileName + ", queryFileName=" + queryFileName + ", nbQueries="
				+ nbQueries + ", readingDataTimeInMilliseconds=" + readingDataTimeInMilliseconds
				+ ", readingQueriesTimeInMilliseconds=" + readingQueriesTimeInMilliseconds
				+ ", evaluationQueriesTimeInMilliseconds=" + evaluationQueriesTimeInMilliseconds
				+ ", evaluationWorkLoadTimeInMilliseconds=" + evaluationWorkLoadTimeInMilliseconds
				+ ", totalTimeInMilliseconds=" + totalTimeInMilliseconds + ", rendement=" + rendement + "]";
	}
	
	
	

}
