package metrics;

public class QEngineMetricsAvg {
	private String dataFile;
	private String queryFile;
	private String nbTripletRDF;
	private String nbQueries;
	private String timeReadDataInMilliseconds;
	private String timeReadQueryInMilliseconds;
	private String timeCreateDictionaryInMilliseconds;
	private String nbIndex;
	private String timeTotalCreateIndexesInMilliseconds;
	private String timeEvaluationQueriesInMilliseconds;
	private String timeEvaluationWorkLoadInMilliseconds;
	private String timeTotalExecutionInMilliseconds;
	private String  rendement;
	
	public QEngineMetricsAvg() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public QEngineMetricsAvg(String dataFile, String queryFile, String nbTripletRDF, String nbQueries,
			String timeReadDataInMilliseconds, String timeReadQueryInMilliseconds,
			String timeCreateDictionaryInMilliseconds, String nbIndex, String timeTotalCreateIndexesInMilliseconds,
			String timeEvaluationQueriesInMilliseconds, String timeEvaluationWorkLoadInMilliseconds,
			String timeTotalExecutionInMilliseconds, String rendement) {
		super();
		this.dataFile = dataFile;
		this.queryFile = queryFile;
		this.nbTripletRDF = nbTripletRDF;
		this.nbQueries = nbQueries;
		this.timeReadDataInMilliseconds = timeReadDataInMilliseconds;
		this.timeReadQueryInMilliseconds = timeReadQueryInMilliseconds;
		this.timeCreateDictionaryInMilliseconds = timeCreateDictionaryInMilliseconds;
		this.nbIndex = nbIndex;
		this.timeTotalCreateIndexesInMilliseconds = timeTotalCreateIndexesInMilliseconds;
		this.timeEvaluationQueriesInMilliseconds = timeEvaluationQueriesInMilliseconds;
		this.timeEvaluationWorkLoadInMilliseconds = timeEvaluationWorkLoadInMilliseconds;
		this.timeTotalExecutionInMilliseconds = timeTotalExecutionInMilliseconds;
		this.rendement = rendement;
	}
	
	public String getDataFile() {
		return dataFile;
	}
	public void setDataFile(String dataFile) {
		this.dataFile = dataFile;
	}
	public String getQueryFile() {
		return queryFile;
	}
	public void setQueryFile(String queryFile) {
		this.queryFile = queryFile;
	}
	public String getNbTripletRDF() {
		return nbTripletRDF;
	}
	public void setNbTripletRDF(String nbTripletRDF) {
		this.nbTripletRDF = nbTripletRDF;
	}
	public String getNbQueries() {
		return nbQueries;
	}
	public void setNbQueries(String nbQueries) {
		this.nbQueries = nbQueries;
	}
	public String getTimeReadDataInMilliseconds() {
		return timeReadDataInMilliseconds;
	}
	public void setTimeReadDataInMilliseconds(String timeReadDataInMilliseconds) {
		this.timeReadDataInMilliseconds = timeReadDataInMilliseconds;
	}
	public String getTimeReadQueryInMilliseconds() {
		return timeReadQueryInMilliseconds;
	}
	public void setTimeReadQueryInMilliseconds(String timeReadQueryInMilliseconds) {
		this.timeReadQueryInMilliseconds = timeReadQueryInMilliseconds;
	}
	public String getTimeCreateDictionaryInMilliseconds() {
		return timeCreateDictionaryInMilliseconds;
	}
	public void setTimeCreateDictionaryInMilliseconds(String timeCreateDictionaryInMilliseconds) {
		this.timeCreateDictionaryInMilliseconds = timeCreateDictionaryInMilliseconds;
	}
	public String getNbIndex() {
		return nbIndex;
	}
	public void setNbIndex(String nbIndex) {
		this.nbIndex = nbIndex;
	}
	public String getTimeTotalCreateIndexesInMilliseconds() {
		return timeTotalCreateIndexesInMilliseconds;
	}
	public void setTimeTotalCreateIndexesInMilliseconds(String timeTotalCreateIndexesInMilliseconds) {
		this.timeTotalCreateIndexesInMilliseconds = timeTotalCreateIndexesInMilliseconds;
	}
	public String getTimeEvaluationQueriesInMilliseconds() {
		return timeEvaluationQueriesInMilliseconds;
	}
	public void setTimeEvaluationQueriesInMilliseconds(String timeEvaluationQueriesInMilliseconds) {
		this.timeEvaluationQueriesInMilliseconds = timeEvaluationQueriesInMilliseconds;
	}
	public String getTimeEvaluationWorkLoadInMilliseconds() {
		return timeEvaluationWorkLoadInMilliseconds;
	}
	public void setTimeEvaluationWorkLoadInMilliseconds(String timeEvaluationWorkLoadInMilliseconds) {
		this.timeEvaluationWorkLoadInMilliseconds = timeEvaluationWorkLoadInMilliseconds;
	}
	public String getTimeTotalExecutionInMilliseconds() {
		return timeTotalExecutionInMilliseconds;
	}
	public void setTimeTotalExecutionInMilliseconds(String timeTotalExecutionInMilliseconds) {
		this.timeTotalExecutionInMilliseconds = timeTotalExecutionInMilliseconds;
	}
	public String getRendement() {
		return rendement;
	}
	public void setRendement(String rendement) {
		this.rendement = rendement;
	}

	@Override
	public String toString() {
		return "QEngineMetricsAvg [dataFile=" + dataFile + ", queryFile=" + queryFile + ", nbTripletRDF=" + nbTripletRDF
				+ ", nbQueries=" + nbQueries + ", timeReadDataInMilliseconds=" + timeReadDataInMilliseconds
				+ ", timeReadQueryInMilliseconds=" + timeReadQueryInMilliseconds
				+ ", timeCreateDictionaryInMilliseconds=" + timeCreateDictionaryInMilliseconds + ", nbIndex=" + nbIndex
				+ ", timeTotalCreateIndexesInMilliseconds=" + timeTotalCreateIndexesInMilliseconds
				+ ", timeEvaluationQueriesInMilliseconds=" + timeEvaluationQueriesInMilliseconds
				+ ", timeEvaluationWorkLoadInMilliseconds=" + timeEvaluationWorkLoadInMilliseconds
				+ ", timeTotalExecutionInMilliseconds=" + timeTotalExecutionInMilliseconds + ", rendement=" + rendement
				+ "]";
	}
	
	
}
