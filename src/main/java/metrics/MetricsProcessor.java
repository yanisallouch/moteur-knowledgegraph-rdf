package metrics;

import java.util.LinkedList;
import java.util.List;
import java.util.OptionalDouble;

public class MetricsProcessor {

	public JenaMetricsAvg getJenaMetricAvg(List<JenaMetric> jenaMetrics) {
		List <Long> readingDataTimesList =  new LinkedList<Long>();
		List <Long> readingQueriesTimesList =  new LinkedList<Long>();
		List <Long> evaluationQueriesTimesList =  new LinkedList<Long>(); 
		List <Long> evaluationWorkLoadTimesList =  new LinkedList<Long>();
		List <Long> totalExecutionTimesList =  new LinkedList<Long>();
		List <Double> rendementsList =  new LinkedList<Double>();
		
		
		double readingDataTimeAvg;
		double readingQueriesTimeAvg;
		double evaluationQueriesTimeAvg;
		double evaluationWorkLoadTimeAvg;
		double totalTimeAvg;
		double rendementAvg;
		
		for (JenaMetric jenaMetric : jenaMetrics) {
			readingDataTimesList.add(jenaMetric.getReadingDataTimeInMilliseconds());
			readingQueriesTimesList.add(jenaMetric.getReadingQueriesTimeInMilliseconds());
			evaluationQueriesTimesList.add(jenaMetric.getEvaluationQueriesTimeInMilliseconds());
			evaluationWorkLoadTimesList.add(jenaMetric.getEvaluationWorkLoadTimeInMilliseconds());
			totalExecutionTimesList.add(jenaMetric.getTotalTimeInMilliseconds());
			rendementsList.add(jenaMetric.getRendement());	
		}
		
		readingDataTimeAvg = getMoyReobuste(readingDataTimesList);
		readingQueriesTimeAvg = getMoyReobuste(readingQueriesTimesList);
		evaluationQueriesTimeAvg= getMoyReobuste(evaluationQueriesTimesList);
		evaluationWorkLoadTimeAvg = getMoyReobuste(evaluationWorkLoadTimesList);
		totalTimeAvg = getMoyReobuste(totalExecutionTimesList);
		rendementAvg = getMoyReobuste2(rendementsList);
		
		JenaMetricsAvg jenaMetricsAvg = new JenaMetricsAvg(jenaMetrics.get(0).getDataFileName(),
				jenaMetrics.get(0).getQueryFileName(), String.valueOf(jenaMetrics.get(0).getNbQueries()),
				String.valueOf(readingDataTimeAvg),String.valueOf(readingQueriesTimeAvg) ,
				String.valueOf(evaluationQueriesTimeAvg), String.valueOf(evaluationWorkLoadTimeAvg),
				String.valueOf(totalTimeAvg), String.valueOf(rendementAvg));
		
		
		return jenaMetricsAvg;
	}

	private double getMoyReobuste(List<Long> lst) {
		lst.sort((o1, o2) -> o1.compareTo(o2));
		lst.remove(0);
		int lastElt = lst.size()-1;
		lst.remove(lastElt);
		OptionalDouble moyRob = lst.stream().mapToDouble(a -> a).average();
		double moyRobV2 =  moyRob.getAsDouble();
		moyRobV2 =  (double) Math.round(moyRobV2 * 100) / 100;
		return moyRobV2;
		
	}
	
	private double getMoyReobuste2(List<Double> lst) {
		lst.sort((o1, o2) -> o1.compareTo(o2));
		lst.remove(0);
		int lastElt = lst.size()-1;
		lst.remove(lastElt);
		OptionalDouble moyRob = lst.stream().mapToDouble(a -> a).average();
		double moyRobV2 =  moyRob.getAsDouble();
		moyRobV2 =  (double) Math.round(moyRobV2 * 100) / 100;
		return moyRobV2;
		
	}
	
	public QEngineMetricsAvg getQEngineMetricsAvg (List<QEngineMetrics> qEngineMetrics) {
		
		
		List <Long> readingDataTimesList =  new LinkedList<Long>();
		List <Long> readingQueriesTimesList =  new LinkedList<Long>();
		List<Long>  creatingDictionaryTimesList =  new LinkedList<Long>();
		List<Long>  creatingIndexesTimesList = new LinkedList<Long>();
		List <Long> evaluationQueriesTimesList =  new LinkedList<Long>(); 
		List <Long> evaluationWorkLoadTimesList =  new LinkedList<Long>();
		List <Long> totalExecutionTimesList =  new LinkedList<Long>();
		List <Double> rendementsList =  new LinkedList<Double>();
		
		
		double readingDataTimeAvg;
		double readingQueriesTimeAvg;
		double creatingDictionaryTimesAvg;
		double creatingIndexesTimesAvg;
		double evaluationQueriesTimeAvg;
		double evaluationWorkLoadTimeAvg;
		double totalExecutionTimeAvg;
		double rendementAvg;
		
		for(QEngineMetrics qEM : qEngineMetrics) {
			readingDataTimesList.add(qEM.getTimeReadDataInMilliseconds());
			readingQueriesTimesList.add(qEM.getTimeReadQueryInMilliseconds());
			creatingDictionaryTimesList.add(qEM.getTimeCreateDictionaryInMilliseconds());
			creatingIndexesTimesList.add(qEM.getTimeTotalCreateIndexesInMilliseconds());
			evaluationQueriesTimesList.add(qEM.getTimeEvaluationQueriesInMilliseconds());
			evaluationWorkLoadTimesList.add(qEM.getTimeEvaluationWorkLoadInMilliseconds());
			totalExecutionTimesList.add(qEM.getTimeTotalExecutionInMilliseconds());
			rendementsList.add(qEM.getRendement());
		}
		
	
		
		readingDataTimeAvg = getMoyReobuste(readingDataTimesList);
		readingQueriesTimeAvg = getMoyReobuste(readingQueriesTimesList);
		creatingDictionaryTimesAvg = getMoyReobuste(creatingDictionaryTimesList);
		creatingIndexesTimesAvg = getMoyReobuste(creatingIndexesTimesList);
		evaluationQueriesTimeAvg = getMoyReobuste(evaluationQueriesTimesList);
		evaluationWorkLoadTimeAvg = getMoyReobuste(evaluationWorkLoadTimesList);
		totalExecutionTimeAvg = getMoyReobuste(totalExecutionTimesList);
		rendementAvg = getMoyReobuste2(rendementsList);
		
		QEngineMetricsAvg qEngineMAvg = new QEngineMetricsAvg(qEngineMetrics.get(0).getDataFile(),qEngineMetrics.get(0).getQueryFile(), qEngineMetrics.get(0).getNbTripletRDF(), qEngineMetrics.get(0).getNbQueries(),String.valueOf(readingDataTimeAvg),String.valueOf(readingQueriesTimeAvg),String.valueOf(creatingDictionaryTimesAvg), qEngineMetrics.get(0).getNbIndex(),String.valueOf(creatingIndexesTimesAvg),String.valueOf(evaluationQueriesTimeAvg),String.valueOf(evaluationWorkLoadTimeAvg),String.valueOf(totalExecutionTimeAvg),String.valueOf(rendementAvg));
		
		
		
		return qEngineMAvg;
	
	}

}
