package dictionary;

import java.util.HashMap;

/**
 * @author KACI Ahmed
 * @author ALLOUCH Yanis
 */
public class Dictionary {

	private HashMap<String, Long> dicoMap;
	private HashMap<Long, String> inverseDicoMap;
	long nbTripletRDF = 0;

	public Dictionary() {
		super();
		this.dicoMap = new HashMap<String, Long>();
		this.inverseDicoMap = new HashMap<Long, String>();
	}

	public boolean containsElement(String elt) {
		if (dicoMap.keySet().contains(elt)) {
			return true;
		}
		return false;
	}

	public void addElement(String elt, long cpt) {
		dicoMap.put(String.valueOf(elt), Long.valueOf(cpt));
		inverseDicoMap.put(Long.valueOf(cpt), String.valueOf(elt));
		nbTripletRDF++;
	}

	public long getIndexOfElement(String elt) {
		if (dicoMap.get(elt) != null) {
			return Long.valueOf(dicoMap.get(elt));
		}
		return -1;
	}

	public String getElementOfIndex(long index) {
		return String.valueOf(inverseDicoMap.get(index));
	}

	public void displayDictionnary() {
		for (String elt : dicoMap.keySet()) {
			System.out.println("(" + elt + " , " + dicoMap.get(elt) + " )");
		}
	}

	public long getNbTripletRDF() {
		return nbTripletRDF;
	}

	public void setNbTripletRDF(long nbTripletRDF) {
		this.nbTripletRDF = nbTripletRDF;
	}
}
